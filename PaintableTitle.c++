
#include "PaintableTitle.h"
#include "TfmAxisText.h"
#include "Configuration.h"

void PaintableTitle::paint(cairo_t * c, std::ostream & os) const {
  if (c) {
  TfmAxisText tst;
  tst.paint(c, os);

  cairo_font_extents_t CFE;
  cairo_font_extents(c, &CFE);
  cairo_text_extents_t CTE;

  cairo_text_extents(c, m_title.c_str(), &CTE);  
  cairo_move_to(c, 0.5 - CTE.width/2, 1.0 + CFE.height*2);
  Configuration::setFGColour(c);
  cairo_show_text(c, m_title.c_str());
  
  }
} 
