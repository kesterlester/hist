#ifndef LESTER_HIST_BARRIER
#define LESTER_HIST_BARRIER

#include <condition_variable>
#include <functional>
#include <mutex>

class Barrier {
 private:
  std::mutex _mutex;
  std::condition_variable _cv;
  std::size_t _count;

 private:
  // If the lab had a gcc version with enough support for lambda functions (4.5
  // or higher), we would not need "workaround":
  bool workaround() const { return _count == 0; }

 public:
  explicit Barrier(std::size_t count) : _count{count} {}
  void wait() {
    std::unique_lock<std::mutex> lock{_mutex};
    if (--_count == 0) {
      _cv.notify_all();
    } else {
      //_cv.wait(lock, [this] { return _count == 0; });   // doesn't work in lab
      //with gcc 4.4, so substitute for line below
      _cv.wait(lock, std::bind(&Barrier::workaround, this));
    }
  }
};

#endif
