
#ifndef LESTER_TFMPROFILEHIST_H
#define LESTER_TFMPROFILEHIST_H

#include "CairoPaintable.h"
#include "TfmHistBox.h"

class ProfileHist;

class TfmProfileHist : public CairoPaintable {
 public:
  TfmProfileHist(const ProfileHist & hist);
  void paint(cairo_t * c, std::ostream & os) const;
 private:
  TfmHistBox m_tfmHistBox;
};

#endif
