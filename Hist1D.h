
#ifndef LESTER_HIST1D_H
#define LESTER_HIST1D_H

#include <map>
#include <iostream>

#include "Hist.h"
#include "Axis.h"

class Hist1D : public Hist {
 public:
  Hist1D(const Axis & xAxis) ;

  void fill(double x, double weight=1);
  void fillOverwrite(double x, double weight=1);
  void dumpTo(std::ostream & os, const std::string * label=0) const;
  bool normalize();
  bool cumulate();
  // return a histogram containing histA/histB.  Precondition: binning same for both
  static Hist1D divide(const Hist1D & histA, const Hist1D & histB);
  double weightForBinNumber(const int n) const;

// Data members:

  Axis xAxis;
  typedef std::map<Bin, double> Data;
  Data data;
};

# endif





