
#include "RainbowPalette.h"

RainbowPalette::RainbowPalette() {
  setInterpolant(0.00,Colour(0,0,1));
  setInterpolant(0.25,Colour(0,1,1));
  setInterpolant(0.35,Colour(0,1,0.8));
  setInterpolant(0.40,Colour(0,1,0));
  setInterpolant(0.60,Colour(0.9,0.9,0));
  setInterpolant(0.75,Colour(1,1,0));
  setInterpolant(1.00,Colour(1,0,0));
}
