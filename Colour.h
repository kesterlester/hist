
#ifndef LESTER_COLOUR_H
#define LESTER_COLOUR_H

#include "CairoPaintable.h"
#include <iostream>

class Colour : public CairoPaintable {
 public:
  Colour() : red(0), green(0), blue(0) {}
  Colour(double red, double green, double blue) :
    red(red), green(green), blue(blue) {
  }
  void moveTowards(const Colour & other);
  virtual void paint(cairo_t * c, std::ostream & os) const;
  virtual void paint(cairo_t * c, std::ostream & os, const double gamma) const;
  double red,green,blue;
  static Colour setColourFromIndex(unsigned int colour);
};

inline std::ostream & operator<<(std::ostream & os, const Colour & colour) {
  return os << "Colour[R:" << colour.red << ", G:" << colour.green << ", B:" << colour.blue << "]";
}

#endif

