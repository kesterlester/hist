
#ifndef LESTER_TFMAXISTEXT_H
#define LESTER_TFMAXISTEXT_H

#include "CairoPaintable.h"

// Seem to need this class to keep text from becoming upside down when we make y run up the page for histograms!!!!!

class TfmAxisText : public CairoPaintable {
 public:
  void paint(cairo_t * c, std::ostream & os) const;
 private:
};

#endif
