
#include "Background.h"
#include "Configuration.h"

//#include "tmpTfms.h"

//#include <iostream>

void Background::paint(cairo_t * c, std::ostream & os) const {

  //std::mes() << __FILE__ << " " << __FUNCTION__ << std::endl;

  if (c) {
  cairo_rectangle(c, 0,0,Configuration::SIZEX,Configuration::SIZEY);
  // cairo_rectangle(c, tfx(0), tfy(0), tfx(1)-tfx(0), tfy(1)-tfy(0));
  if (m_r>0.f && m_g>0.f && m_b>0.f) {
    cairo_set_source_rgb(c, m_r, m_g, m_b);
  } else {
    Configuration::setBGColour(c);
  }
  cairo_fill(c);


  //cairo_set_source_rgb(c, m_r, m_g, m_b);
  //cairo_move_to(c, tfx(0), tfy(0));
  //cairo_line_to(c, tfx(1), tfy(1));
  //cairo_stroke(c);
  }
}
