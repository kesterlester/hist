
#include "PaintableProfileHistogram.h"
#include "TfmAxisText.h"
#include "TfmProfileHist.h"
#include "PaintableAxes.h"
#include "PaintableLegend.h"
#include "PaintableTitle.h"
#include "ThePalette.h"
#include "LinearlyRemappedPalette.h"
#include "Configuration.h"
#include "ColourScale.h"
#include "ProfileHist.h"
#include "LogScaleHelper.h"

const double transparencyLevel=0.5;

void PaintableProfileHistogram::paint(cairo_t * c, std::ostream & os) const {

  if (m_profileHistPtrMap.empty()) {
    return;
  }

  
  const bool trueMulti = Configuration::multi && !(Configuration::ratio);

  // Now loop over all histograms in the edited map:

  std::vector<Data::Kind> types;
  std::vector<Colour> colours;
  int nPlots = -1;
  for (Data::ProfileHistPtrMap::const_iterator it = m_profileHistPtrMap.begin();
          it != m_profileHistPtrMap.end();
          ++it) {
      ++nPlots;
      const Data::Kind kind = (it->first);
      types.push_back(kind);
      int colour = 0;
      for (Data::Kinds::const_iterator jit = Data::kinds.begin();
              jit != Data::kinds.end();
              ++jit) {
          if ((*jit)==it->first) {
             break;
          }
          colour++;
      }
      const bool isFirstPlot = (nPlots==0);
      const Colour col = Colour::setColourFromIndex(colour);
      colours.push_back(col); // for legend
  const ProfileHist & hist = *(it->second); // FIXME -- need better transparency, sometimes

  const bool needTransparency = trueMulti && m_profileHistPtrMap.size()>1 && !(Configuration::overwrite) ;
  const bool needMultiColour = trueMulti && m_profileHistPtrMap.size()>1 && (Configuration::overwrite) ;

  // If our palette has to adapt to data, then it is not until THIS point that we can be sure what mappings (if any) will have to be applied to our "basic" palette.
  /// START TO DETERMINE PALETTE
  std::shared_ptr<Palette> palette = ThePalette::get(); 


  double smallestWeightToPlot;
  double largestWeightToPlot;

  findLargestAndSmallestWeightToPlot(false, hist, smallestWeightToPlot, largestWeightToPlot);

  /*
  if (Configuration::haveLogScaleZ) {
    // Log Scale Z !!
    // What is smallest weight to plot?
    // Start out with some defaults that are at least plottable, then correct them.
    smallestWeightToPlot = 0.1;
    largestWeightToPlot = 1;
    if (Configuration::calcuz==true && Configuration::calclz==true) {
      // auto both top and bot
      if (hist.maxWeight>0) {
  largestWeightToPlot=hist.maxWeight;
      }
      if (hist.smallestPosWeight>0 && hist.smallestPosWeight<hist.maxWeight) {
  smallestWeightToPlot = hist.smallestPosWeight;
      } else {
  smallestWeightToPlot = largestWeightToPlot/10;
      }
    } else if (Configuration::calcuz==true) {
      // auto just top, using supplied bot
      if (Configuration::lz>0) {
  smallestWeightToPlot = Configuration::lz; // otherwise stick with default.
      }
      if (hist.maxWeight>smallestWeightToPlot) {
  largestWeightToPlot = hist.maxWeight;
      } else {
  largestWeightToPlot = smallestWeightToPlot*10;
      }
    } else if (Configuration::calcuz==true) {
      // auto just bot, using supplied top
       if (Configuration::uz>0) {
  largestWeightToPlot = Configuration::uz; // otherwise stick with default.
      }
       if (hist.smallestPosWeight>0 && hist.smallestPosWeight<largestWeightToPlot) {
  smallestWeightToPlot = hist.smallestPosWeight;
      } else {
  smallestWeightToPlot = largestWeightToPlot/10;
      }
    } else {
      // use supplied values for both top and bot,
      // but check they are valid.  If not valid, continue to use default.
      if (Configuration::uz>Configuration::lz && Configuration::lz>0) {
  largestWeightToPlot=Configuration::uz;
  smallestWeightToPlot=Configuration::lz;
      }
    }


  } else {
    // Linear Scale Z !!

    // Start out with some defaults that are at least plottable, then correct them.
    smallestWeightToPlot = 0;
    largestWeightToPlot = 1;

    if (Configuration::calcuz==true && Configuration::calclz==true) {
      // auto both top and bot
      largestWeightToPlot = hist.maxWeight;
      smallestWeightToPlot = hist.minWeight;
      if (hist.maxWeight == hist.minWeight) {
  // fix up bad range
  if (hist.maxWeight>0) {
    smallestWeightToPlot = 0;
  } else {
    largestWeightToPlot = 1;
  }
      }
    } else if (Configuration::calcuz==true) {
      // auto just top, using supplied bot
      smallestWeightToPlot = Configuration::lz;
      if (hist.maxWeight>smallestWeightToPlot) {
  largestWeightToPlot = hist.maxWeight;
      } else {
  largestWeightToPlot = smallestWeightToPlot+1;
      }
    } else if (Configuration::calcuz==true) {
      // auto just bot, using supplied top
      largestWeightToPlot = Configuration::uz;
      if (hist.minWeight<largestWeightToPlot) {
  if (hist.minWeight<0) {
    smallestWeightToPlot = hist.minWeight;
  } else {
    smallestWeightToPlot=0;
  }
      } else {
  if (largestWeightToPlot>0) {
    smallestWeightToPlot=0;
  } else {
    smallestWeightToPlot = largestWeightToPlot-1;
  }
      }
    } else {
      // use supplied values for both top and bot,
      // but check they are valid.  If not valid, continue to use default.
      if (Configuration::uz>Configuration::lz) {
  largestWeightToPlot=Configuration::uz;
  smallestWeightToPlot=Configuration::lz;
      }
    }



  }
  */
  // OK ... now smallest and largestWeightToPlot have been decided!



  // REMAP 0:1 palette to 0:maxWeight palette if desired


  if (Configuration::scaleWeightsToUnitIntervalWhenConvertingToColours) {

    if (Configuration::haveLogScaleZ) {
      // Log Z
      //std::cerr << "LOG rescaling palette" << std::endl;
      palette = std::shared_ptr<LinearlyRemappedPalette>(new LinearlyRemappedPalette(0, log(smallestWeightToPlot)/log(10.), 1, log(largestWeightToPlot)/log(10.), palette) );
    } else {
      // Linear z
      //std::cerr << "LIN rescaling palette" << std::endl;
      palette = std::shared_ptr<LinearlyRemappedPalette>(new LinearlyRemappedPalette(0, smallestWeightToPlot, 1, largestWeightToPlot, palette) );
    }
  } else {
    //std::cerr << "NOT rescaling palette" << std::endl;
  }


  TfmProfileHist tfm(hist);
  tfm.paint(c,os);
  TfmAxisText tat;

  if (c) {

    if (Configuration::fillVoid && !Configuration::haveLogScaleZ) {
         // Blank the space under the canvas according to weight 0
         if (isFirstPlot) {
             ThePalette::get()->getColourFrom(0).paint(c,os);
             cairo_move_to(c, hist.xAxis.leftMostExtent(),  hist.yAxis.leftMostExtent());
             cairo_line_to(c, hist.xAxis.leftMostExtent(),  hist.yAxis.rightMostExtent());
             cairo_line_to(c, hist.xAxis.rightMostExtent(), hist.yAxis.rightMostExtent());
             cairo_line_to(c, hist.xAxis.rightMostExtent(), hist.yAxis.leftMostExtent());
             // implicit close! :)
             cairo_fill(c);
          }
    }
    if (Configuration::fillVoid && Configuration::haveLogScaleZ) {
      // Represent log(0)=-infty by red cross-hatching ...
      const int bits=50;
      for (int n=0; n<=bits; ++n) {
        const double lam=static_cast<double>(n)/static_cast<double>(bits);
        const double x1=lam*(hist.xAxis.leftMostExtent()) + (1.-lam)*(hist.xAxis.rightMostExtent());
        const double x2=lam*(hist.xAxis.rightMostExtent()) + (1.-lam)*(hist.xAxis.leftMostExtent());
        const double y1=lam*(hist.yAxis.leftMostExtent()) + (1.-lam)*(hist.yAxis.rightMostExtent());
        const double y2=lam*(hist.yAxis.rightMostExtent()) + (1.-lam)*(hist.yAxis.leftMostExtent());

        if (needTransparency) {
           Colour(1,0,0).paint(c,os,transparencyLevel); // red
        } else {
           Colour(1,0,0).paint(c,os); // red
        }
        cairo_move_to(c, x1, hist.yAxis.leftMostExtent());
        cairo_line_to(c,hist.xAxis.leftMostExtent() , y1);
        cairo_move_to(c, x1, hist.yAxis.leftMostExtent());
        cairo_line_to(c,hist.xAxis.rightMostExtent() , y2);

        if (n!=0 && n!=bits) {
          cairo_move_to(c, x2, hist.yAxis.rightMostExtent());
          cairo_line_to(c,hist.xAxis.rightMostExtent() , y2);
          cairo_move_to(c, x2, hist.yAxis.rightMostExtent());
          cairo_line_to(c,hist.xAxis.leftMostExtent() , y1);
        }
      }
      cairo_stroke(c); // I moved this one brace up ... I hope that is oK
    }
  }

  if (!c) {
    os << "#binXMin #binXMax #binYMin #binYMax #weight\n";
  }
  //cairo_set_source_rgb(c, 0.0, 0.0, .0);
  for (ProfileHist::Data::const_iterator it = hist.data.begin();
       it != hist.data.end();
       ++it) {
    const ProfileHist::Bin2D & bin2D = it->first;
    const Bin & xBin = bin2D.first;
    const Bin & yBin = bin2D.second;





    const Stats2D & stats = it->second;
    double weight = stats.getSumWeight();

    if (Configuration::haveLogScaleZ) {
      if (weight>0) {
        //static const onLog10=1./log(10.0);
        weight=log(weight)/log(10.);
      } else {
        // weight <=0 socan't take log!
        continue;
      }
    }

    const double xMean = stats.x().getMean();
    const double yMean = stats.y().getMean();

    const double xSD = sqrt(stats.x().getSampleVariance());
    const double ySD = sqrt(stats.y().getSampleVariance());

    //std::cout << "MOO " << xMean << " " << yMean << " " << xSD << " " << ySD << std::endl;
    if(c) {
      tfm.paint(c,os); // TEST

      cairo_move_to(c,xMean-xSD,yMean-ySD);
      cairo_line_to(c,xMean-xSD,yMean+ySD);
      cairo_line_to(c,xMean+xSD,yMean+ySD);
      cairo_line_to(c,xMean+xSD,yMean-ySD);
      cairo_close_path(c);
      if (needTransparency) {
         Colour col2 = palette->getColourFrom(weight);
         col2.moveTowards(col);
         col2.paint(c,os,transparencyLevel);
      } else if (Configuration::fadeProfileUsingWeight)  {
         Colour col2 = palette->getColourFrom(weight);
         col2.paint(c,os);
      }  else {
         col.paint(c,os);
      } 
      //std::cerr << "Doing bin with col  for " << weight << " being " << palette->getColourFrom(weight) << std::endl;
      //cairo_set_source_rgb(c, weight, weight, weight);
      //cairo_fill(c);
      //   col.paint(c,os);
      
      if (c) tat.paint(c,os); // TEST
      if (c) cairo_set_line_width(c,Configuration::graphThickness);

      cairo_stroke(c);
    } else {
      // dump
      os << xBin.from << " " << xBin.to << " " << yBin.from << " " << yBin.to << " " << weight << "\n";
    }
  }


  // Now draw axes.
  PaintableAxes axes(hist.xAxis, hist.yAxis);
  axes.paint(c,os);

  // Now draw title.
  if (Configuration::title != "") {
    PaintableTitle title(Configuration::title);
    title.paint(c,os);
  }

  if (Configuration::colourScale) {

    //const double weightFrom = Configuration::calclz ? (Configuration::scaleWeightsToUnitIntervalWhenConvertingToColours ? 0 : hist.minWeight) : Configuration::lz;
    //const double weightTo   = Configuration::calcuz ? (Configuration::scaleWeightsToUnitIntervalWhenConvertingToColours ? 1 : hist.maxWeight) : Configuration::uz;

    //std::mes() << "WF " << weightFrom << " WT" << weightTo << std::endl;
    ColourScale colourScale(smallestWeightToPlot, largestWeightToPlot, Configuration::haveLogScaleZ, palette);
    colourScale.paint(c,os);
  }

  } // End of big loop over histograms (if multi)

  if (trueMulti) {
    // (6) Now draw legend.
    if (Configuration::showLegend) {
      PaintableLegend legend(types, colours);
      if (c) legend.paint(c,os);
    }
  }

  if (c) cairo_identity_matrix(c);



}



