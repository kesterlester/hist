
#ifndef LESTER_THEBUFFER_H
#define LESTER_THEBUFFER_H

#include <memory>
#include <mutex>
#include <thread>

class CairoPaintable;

class TheBuffer {
 public:
  typedef std::shared_ptr<const CairoPaintable> BufferType;
  static TheBuffer & instance();
  BufferType get() const;
  void set(BufferType newBuffer);
 private:
  TheBuffer();
  BufferType m_buffer;
  mutable std::mutex m_mutex;
};
  
#endif
