
#include "Data.h"

Data::Data1DPtr         Data::data1DPtr;
Data::Data2DPtr         Data::data2DPtr;
Data::Data1DWeightedPtr Data::data1DWeightedPtr;
Data::Data2DWeightedPtr Data::data2DWeightedPtr;
Data::Data1DPtrMap         Data::data1DPtrMap;
Data::Data2DPtrMap         Data::data2DPtrMap;
Data::Data1DWeightedPtrMap Data::data1DWeightedPtrMap;
Data::Data2DWeightedPtrMap Data::data2DWeightedPtrMap;

Data::Hist1DPtr Data::hist1DPtr;
Data::Hist2DPtr Data::hist2DPtr;
Data::ProfileHistPtr Data::profileHistPtr;
Data::Hist1DPtrMap Data::hist1DPtrMap;
Data::Hist2DPtrMap Data::hist2DPtrMap;
Data::ProfileHistPtrMap Data::profileHistPtrMap;

Data::Kinds Data::kinds;

bool Data::hasBeenDrawn=false;
bool Data::final=false;

unsigned long Data::size=0;
const Data::Kind Data::defaultKind = "";
  
BoundMonitor Data::xMonitor;
BoundMonitor Data::yMonitor;

Stats Data::xStats;
Stats Data::yStats;

std::mutex Data::mutex;

