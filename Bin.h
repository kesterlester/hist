
#ifndef LESTER_BIN_H
#define LESTER_BIN_H

    struct Bin {
      Bin(double from, double to) : from(from), to(to) {};
      double from;
      double to;
      bool operator<(const Bin & other) const {
	if (from<other.from) return true;
	if (from>other.from) return false;
	return to<other.to;
      }
    };

# endif
