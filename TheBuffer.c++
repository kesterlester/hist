
#include "TheBuffer.h"
#include "Background.h"

TheBuffer & TheBuffer::instance() {
  static TheBuffer theBuffer;
  return theBuffer;
}

TheBuffer::TheBuffer() {
  // default buffer ... a yellow square;
  this->set(TheBuffer::BufferType(new Background(1,1,0)));
}

TheBuffer::BufferType TheBuffer::get() const {
  std::lock_guard<std::mutex> lock(m_mutex);
  return m_buffer;
}

void TheBuffer::set(TheBuffer::BufferType newBuffer) {
  std::lock_guard<std::mutex> lock(m_mutex);
  if (newBuffer.get()) {
    m_buffer = newBuffer;
  }
}
