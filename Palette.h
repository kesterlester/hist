
#ifndef LESTER_PALETTE_H
#define LESTER_PALETTE_H

#include "Colour.h"

class Palette {
 public:
  virtual Colour getColourFrom(const double weight) const = 0;
  virtual ~Palette() {}
};

#endif
