
#ifndef LESTER_PAINTABLEPROFILEHISTOGRAM_H
#define LESTER_PAINTABLEPROFILEHISTOGRAM_H

#include "CairoPaintableHistogram.h"
#include "Data.h"
#include <memory>

//fwd dec
class ProfileHist;

class PaintableProfileHistogram : public PaintableHistogram {
 public:
  PaintableProfileHistogram(Data::ProfileHistPtrMap profileHistPtrMap) : m_profileHistPtrMap(profileHistPtrMap) {}
  virtual void paint(cairo_t * c, std::ostream & os) const; // dump to os if c is null
 private:
  Data::ProfileHistPtrMap m_profileHistPtrMap;
};

#endif
