#ifndef LESTERSYNCHH
#define LESTERSYNCHH

#include "Barrier.h"
#include <iostream>

static std::mutex m_ksjdfhkjsd;

struct Synch {


   static Barrier & barrier1() {
     std::lock_guard<std::mutex> lk(m_ksjdfhkjsd);
     static Barrier b(2);
     return b;
   }
   static Barrier & barrier2() {
     std::lock_guard<std::mutex> lk(m_ksjdfhkjsd);
     static Barrier b(2);
     return b;
   }
   static Barrier & barrier3() {
     std::lock_guard<std::mutex> lk(m_ksjdfhkjsd);
     static Barrier b(2);
     return b;
   }

  static void waitForXWindowThreadReadyBeforeProceeding() {
    //std::cerr << "Inside XWINDOW wait" << std::endl;
    Barrier & b = barrier1();
    b.wait();
  }
  static void waitForRedrawerThreadReadyBeforeProceeding() {
    //std::cerr << "Inside REDRAWER wait" << std::endl;
    Barrier & b = barrier2();
    b.wait();
  }
  static void waitForMainThreadReadyBeforeProceeding() {
    //std::cerr << "Inside MAINTHREAD wait" << std::endl;
    Barrier & b = barrier3();
    b.wait();
  }
};
#endif
