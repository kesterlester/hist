
#ifndef LESTER_PAINTABLETITLE_H
#define LESTER_PAINTABLETITLE_H

#include "CairoPaintable.h"
#include <string>

class PaintableTitle : public CairoPaintable {
public:
  PaintableTitle(const std::string & title) :
    m_title(title) {
  }
  void paint(cairo_t * c, std::ostream & os) const; 
private:
  std::string m_title;
};

#endif
