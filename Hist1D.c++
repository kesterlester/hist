#include "Hist1D.h"

#include <iostream>


Hist1D::Hist1D(const Axis & axis) : xAxis(axis) {}

void Hist1D::fill(double x, double weight) {
  totWeight += weight;
  const Bin & bin = xAxis.getBin(x);
  const double w = (data[bin] += weight); // creates bin entry if it doesn't exist
  //std::mes() << "+FILL " << x << std::endl;
  updateWeightBounds(w);
}

void Hist1D::fillOverwrite(double x, double weight) {
  const Bin & bin = xAxis.getBin(x);
  const double oldWeight = data[bin];
  data[bin] = weight; // creates bin entry if it doesn't exist
  totWeight += (weight-oldWeight);
  //std::mes() << "=FILL " << x << std::endl;
  updateWeightBounds(weight);
}
void Hist1D::dumpTo(std::ostream & os, const std::string * label) const {
  if (label) {
    os <<"#label ";
  } else {
    os <<"#";
  }
  os << "binXMin #binXMax #weight\n";
  for (unsigned int n=0; n<xAxis.n; ++n) {
    const Bin & xBin = xAxis.getBinByNumber(n);
    if (label) {
      os << (*label) << " ";
    }
    os << xBin.from << " " << xBin.to << " " << weightForBinNumber(n) << "\n";
  }
}
// Make each bin grow, containing the sum of the bins before.  Return false if we cannot do this.
bool Hist1D::cumulate() {
  //std::cout << "First totWeight = " << totWeight << std::endl;
  if ((!(Configuration::normalize)) || (Configuration::normalize && totWeight!=0)) {
    double runningTotalWeight = 0;
    for (Data::iterator it = data.begin();
         it!=data.end();
         ++it) {
      runningTotalWeight += (*it).second;
      if (Configuration::normalize) {
        (*it).second = runningTotalWeight/totWeight;
      } else {
        (*it).second = runningTotalWeight;
      }
    }

    if (Configuration::normalize) {
      minWeight=0;
      maxWeight=1;
      smallestPosWeight/=totWeight ;
      totWeight=1;
    } else {
      minWeight=0;
      maxWeight=totWeight;
    }

    return true; // we succeeded
  } else {
    return false; // we could not normalise
  }
}
bool Hist1D::normalize() {
    // Note that cumulate replaces some of the normalization conventions in 1D histos
  //std::cout << "First totWeight = " << totWeight << std::endl;
  if (totWeight!=0) {
    for (Data::iterator it = data.begin();
         it!=data.end();
         ++it) {
      (*it).second /= totWeight;
    }
    finaliseNormalize();
    //std::cout << " ... then totWeight = " << totWeight << std::endl;
    return true; // we succeeded
  } else {
    return false; // we could not normalise
  }
}
// return a histogram containing histA/histB.  Precondition: binning same for both
Hist1D Hist1D::divide(const Hist1D & histA, const Hist1D & histB) {
  if (histA.xAxis != histB.xAxis) {
    std::cerr << "Incompatible histograms in divide " << __FILE__ << " " << __LINE__ << std::endl;
    throw std::string("Incompatible histograms");
  }
  Hist1D ans(histA.xAxis);
  for (unsigned int n=0; n<histA.xAxis.n; ++n) {

    const Bin & xBin = histA.xAxis.getBinByNumber(n);
    const double weightA = histA.weightForBinNumber(n);
    const double weightB = histB.weightForBinNumber(n);
    const double ratio = weightA/weightB;
    ans.data[xBin]=ratio; // ratio may be infinite ... but still fill histo, even if it is!
    if (std::isfinite(ratio)) { // If it's an inf or a nan, don't update
      ans.updateWeightBounds(ratio);
    }
  }
  return ans;
}
double Hist1D::weightForBinNumber(const int n) const {
  if (n<0 || n>=static_cast<int>(xAxis.n)) {
    return 0;
  } else {
    const Bin & bin = xAxis.getBinByNumber(n);
    const Data::const_iterator & it = data.find(bin);
    if (it==data.end()) {
      return 0;
    } else {
      return it->second;
    }
  }
}

