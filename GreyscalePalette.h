
#ifndef LESTER_GREYSCALEPALETTE_H
#define LESTER_GREYSCALEPALETTE_H

#include "InterpolatedPalette.h"

class GreyscalePalette : public InterpolatedPalette {
 public:
  GreyscalePalette();
};

#endif
