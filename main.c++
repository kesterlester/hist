
#include "OurXWindow.h"

#include <functional>
#include "Configuration.h"
#include "Redrawer.h"
#include "Data.h"
#include <iostream>
#include <cmath>
#include "newHistArgs.h"
#include <cairo/cairo-pdf.h>
#include <cairo/cairo-ps.h>
#include <cairo/cairo-svg.h>
#include "TheBuffer.h"
#include "CairoPaintable.h"
#include "Axis.h"
#include "Hist1D.h"
#include "Hist2D.h"
#include "ProfileHist.h"
#include <fstream>
#include "Synch.h"
#include <unistd.h>
#include <algorithm>

void writepdf(const std::string & fname) {
  std::mes() << "Writing pdf at end with name [" << fname << "] ... " << std::flush;
  cairo_surface_t *cs;

  cs=cairo_pdf_surface_create(fname.c_str(), Configuration::SIZEX, Configuration::SIZEY);

  cairo_t *c=cairo_create(cs);
  TheBuffer::instance().get()->paint(c, std::cerr);
  cairo_show_page(c);
  cairo_destroy(c);

  cairo_surface_flush(cs);
  cairo_surface_destroy(cs);
  std::mes() << "done." << std::endl;
}


void writesvg(const std::string & fname) {
  std::mes() << "Writing svg at end with name [" << fname << "] ... " << std::flush;
  cairo_surface_t *cs;

  cs=cairo_svg_surface_create(fname.c_str(), Configuration::SIZEX, Configuration::SIZEY);

  cairo_t *c=cairo_create(cs);
  TheBuffer::instance().get()->paint(c, std::cerr);
  cairo_show_page(c);
  cairo_destroy(c);

  cairo_surface_flush(cs);
  cairo_surface_destroy(cs);
  std::mes() << "done." << std::endl;
}


void writepng(const std::string & fname) {
  std::mes() << "Writing png at end with name [" << fname << "] ... " << std::flush;
  cairo_surface_t *cs;

  cs=cairo_image_surface_create(CAIRO_FORMAT_ARGB32, Configuration::SIZEX, Configuration::SIZEY);

  cairo_t *c=cairo_create(cs);
  TheBuffer::instance().get()->paint(c, std::cerr);
  cairo_show_page(c);
  cairo_destroy(c);

  cairo_surface_flush(cs);
  cairo_surface_write_to_png(cs, fname.c_str());
  cairo_surface_destroy(cs);
  std::mes() << "done." << std::endl;
}


void writedump(const std::string & fname) {
  std::ofstream f(fname.c_str());
  cairo_t *c=0;
  TheBuffer::instance().get()->paint(c, f);
  f.close();
}

void writeeps(const std::string & fname) {
  std::mes() << "Writing eps at end with name [" << fname << "] ... " << std::flush;
  cairo_surface_t *cs;

  cs=cairo_ps_surface_create(fname.c_str(), Configuration::SIZEX, Configuration::SIZEY);

  cairo_t *c=cairo_create(cs);
  TheBuffer::instance().get()->paint(c, std::cerr);
  cairo_show_page(c);
  cairo_destroy(c);

  cairo_surface_flush(cs);
  cairo_surface_destroy(cs);
  std::mes() << "done." << std::endl;
}

void createNewKindAndSetDataPtrs(const std::string kind) {
  std::lock_guard<std::mutex> lock(Data::mutex);
  if (std::find(Data::kinds.begin(), Data::kinds.end(), kind)==Data::kinds.end()) {
     Data::kinds.push_back(kind);
  }
  if (Configuration::autosizing) {
    // Autosizing so don't need to make any histogram, but need to make right kind of data structures:
    if (Configuration::dim==1) {
      if (Configuration::weighted) {
        // weighted
        Data::data1DWeightedPtr = Data::Data1DWeightedPtr(new Data::Data1DWeighted());
        Data::data1DWeightedPtrMap.insert(Data::Data1DWeightedPtrMap::value_type(kind,Data::data1DWeightedPtr));
      } else {
        // not weighted
        Data::data1DPtr         = Data::Data1DPtr        (new Data::Data1D        ());
        Data::data1DPtrMap.insert(Data::Data1DPtrMap::value_type(kind,Data::data1DPtr));
      }
    } else if (Configuration::dim==2) {
      if (Configuration::weighted) {
        // weighted
        Data::data2DWeightedPtr = Data::Data2DWeightedPtr(new Data::Data2DWeighted());
        Data::data2DWeightedPtrMap.insert(Data::Data2DWeightedPtrMap::value_type(kind,Data::data2DWeightedPtr));
      } else {
        // not weighted
        Data::data2DPtr         = Data::Data2DPtr        (new Data::Data2D        ());
        Data::data2DPtrMap.insert(Data::Data2DPtrMap::value_type(kind,Data::data2DPtr));
      }
    }
  } else {
    // not autosizing.  Need to make right kind of histogram.
    if (Configuration::dim==1) {
      Data::hist1DPtr = std::shared_ptr<Hist1D>(new Hist1D(Axis(Configuration::nx, Configuration::lx, Configuration::ux, false, Configuration::intx)));
      Data::hist1DPtrMap.insert(Data::Hist1DPtrMap::value_type(kind, Data::hist1DPtr));
    } else if (Configuration::dim==2 && Configuration::profile()) {
      Data::profileHistPtr = std::shared_ptr<ProfileHist>(new ProfileHist(Axis(Configuration::nx, Configuration::lx, Configuration::ux, false, Configuration::intx),
                        Axis(Configuration::ny, Configuration::ly, Configuration::uy, false, Configuration::inty)));
      Data::profileHistPtrMap.insert(Data::ProfileHistPtrMap::value_type(kind, Data::profileHistPtr));
    } else if (Configuration::dim==2 && !(Configuration::profile())) {
      Data::hist2DPtr = std::shared_ptr<Hist2D>(new Hist2D(Axis(Configuration::nx, Configuration::lx, Configuration::ux, false, Configuration::intx),
                        Axis(Configuration::ny, Configuration::ly, Configuration::uy, false, Configuration::inty)));
      Data::hist2DPtrMap.insert(Data::Hist2DPtrMap::value_type(kind, Data::hist2DPtr));
    }
  }
}
void setDataPtrsToExistingKind(const std::string kind) {
  if (Configuration::autosizing) {
    // Autosizing so don't need to make any histogram, but need to make right kind of data structures:
    if (Configuration::dim==1) {
      if (Configuration::weighted) {
        // weighted
        Data::data1DWeightedPtr = Data::data1DWeightedPtrMap[kind];
      } else {
        // not weighted
        Data::data1DPtr = Data::data1DPtrMap[kind];
      }
    } else if (Configuration::dim==2) {
      if (Configuration::weighted) {
        // weighted
        Data::data2DWeightedPtr = Data::data2DWeightedPtrMap[kind];
      } else {
        // not weighted
        Data::data2DPtr = Data::data2DPtrMap[kind];
      }
    }
  } else {
    // not autosizing.  Need to make right kind of histogram.
    if (Configuration::dim==1) {
      Data::hist1DPtr = Data::hist1DPtrMap[kind];
    } else if (Configuration::dim==2 && Configuration::profile()) {
      Data::profileHistPtr = Data::profileHistPtrMap[kind];
    } else if (Configuration::dim==2 && !(Configuration::profile())) {
      Data::hist2DPtr = Data::hist2DPtrMap[kind];
    }
  }
}

void mainThread() {

  if (Configuration::multi) {
    // Do nothing: kinds are created on the fly.
  } else {
    createNewKindAndSetDataPtrs(Data::defaultKind); // gets lock on Data::mutex as it modifies maps, .. but doesn't edit or add data vectors
  }

  std::string kind; // eg "signal", or "bg", or "cars", or "Spain"
  double x;
  double y;
  double w;

  Synch::waitForMainThreadReadyBeforeProceeding();

  while(!(std::cin.eof())) {

    if (Configuration::multi) {
      if(!(std::cin >> kind)) {
        std::mes() << "Read error on pipe.  Expected a 'kind'.  Didn't get one." << std::endl;
        break; // don't delete the test -- it ensures that incomplete data doesn't cause a fake last entry
      }
    }
    if (Configuration::dim>=1) {
      if(!(std::cin >> x)) {
        std::mes() << "Read error on pipe.  Expected an x-value.  Didn't get one." << std::endl;
        break; // don't delete the test -- it ensures that incomplete data doesn't cause a fake last entry
      }
    }
    if (Configuration::dim>=2) {
      if(!(std::cin >> y)) {
        std::mes() << "Read error on pipe.  Expected a y-value.  Didn't get one." << std::endl;
        break; // don't delete the test -- it ensures that incomplete data doesn't cause a fake last entry
      }
    }
    if (Configuration::weighted) {
      if(!(std::cin >> w)) {
        std::mes() << "Read error on pipe.  Expected a weight.  Didn't get one." << std::endl;
        break; // don't delete the test -- it ensures that incomplete data doesn't cause a fake last entry
      }
    }

    // The above input blocking stuff is all potentially slow -- so we don't want to get any locks during them ...

    if (Configuration::multi) {
      const Data::Kinds::const_iterator it = std::find(Data::kinds.begin(), Data::kinds.end(), kind); // no lock required, as no one else is WRITING to kinds other than us

      if (it == Data::kinds.end()) {
        // this is a new kind of data
        createNewKindAndSetDataPtrs(kind); // gets lock on Data::mutex as it modifies maps, .. but doesn't edit or add data vectors
      } else {
        setDataPtrsToExistingKind(kind);
      }
    }

    // By this point, the appropriate thing for receiving the data, be it hist1DPtr, hist2DPtr, data1DPtr. data1DWeightedPtr, data2DPtr, or data2DWeightedPtr, should now be "ready".

    // Since we are now going to fiddle with the data store:
    std::lock_guard<std::mutex> lock(Data::mutex);

    if (Configuration::weighted) {
      // weighted
      if (Configuration::dim==1) {
        // 1d weighted
        Data::xStats.note(x, w);                       // want mean, rms etc for all data
        if (Configuration::outOfBoundsX(x)) continue;  // ascertain whether clipped ...
        if (Configuration::autosizing) {
          Data::xMonitor.note(x);
          Data::data1DWeightedPtr->push_back(Data::Point1DWeighted(x,w));
        } else {
          if (Configuration::overwrite) {
            if (Configuration::ignoreweights) {
              Data::hist1DPtr->fillOverwrite(x);
            } else {
              Data::hist1DPtr->fillOverwrite(x, w);
            }
          } else {
            Data::hist1DPtr->fill(x,w);
          }
        }
        ++Data::size;
        Data::hasBeenDrawn = false;
      } else {
        // 2d weighted
        Data::xStats.note(x, w);                          // want mean, rms etc for all data
        Data::yStats.note(y, w);                          // want mean, rms etc for all data
        if (Configuration::outOfBoundsXY(x, y)) continue; // ascertain whether clipped ...
        if (Configuration::autosizing) {
          Data::xMonitor.note(x);
          Data::yMonitor.note(y);
          Data::data2DWeightedPtr->push_back(Data::Point2DWeighted(x,y,w));
        } else {
    

	  if (Configuration::profile()) {

                  if (Configuration::overwrite) {
                    if (Configuration::ignoreweights) {
                      Data::profileHistPtr->fillOverwrite(x,y);
                    } else {
                      Data::profileHistPtr->fillOverwrite(x, y, w);
                    }
                  } else {
                    Data::profileHistPtr->fill(x,y,w);
                  }

	  } else {
		  // not profile

                  if (Configuration::overwrite) {
                    if (Configuration::ignoreweights) {
                      Data::hist2DPtr->fillOverwrite(x,y);
                    } else {
                      Data::hist2DPtr->fillOverwrite(x, y, w);
                    }
                  } else {
                    Data::hist2DPtr->fill(x,y,w);
                  }

	  }


        }
        ++Data::size;
        Data::hasBeenDrawn = false;
      }
    } else {
      // not weighted
      if (Configuration::dim==1) {
        // 1d normal
        Data::xStats.note(x);
        if (Configuration::outOfBoundsX(x)) continue;
        if (Configuration::autosizing) {
          Data::xMonitor.note(x);
          Data::data1DPtr->push_back(Data::Point1D(x));
        } else {
          if (Configuration::overwrite) {
            Data::hist1DPtr->fillOverwrite(x);
          } else {
            Data::hist1DPtr->fill(x);
          }
        }
        ++Data::size;
        Data::hasBeenDrawn = false;
      } else {
        // 2d normal
        Data::xStats.note(x);
        Data::yStats.note(y);
        if (Configuration::outOfBoundsXY(x, y)) continue;
        if (Configuration::autosizing) {
          Data::xMonitor.note(x);
          Data::yMonitor.note(y);
          Data::data2DPtr->push_back(Data::Point2D(x,y));
        } else {
	  if (Configuration::profile()) {
                  if (Configuration::overwrite) {
                    Data::profileHistPtr->fillOverwrite(x,y);
                  } else {
                    Data::profileHistPtr->fill(x,y);
                  }
	  } else {
		  // not profile
                  if (Configuration::overwrite) {
                    Data::hist2DPtr->fillOverwrite(x,y);
                  } else {
                    Data::hist2DPtr->fill(x,y);
                  }
	  }
        }
        ++Data::size;
        Data::hasBeenDrawn = false;
      }
    }


  }
  std::mes() << " ==== END ==== " << std::endl; //  This happens when the input ends normally, or when the input stream is corrupted." << std::endl;
  //Monitor::instance().notifyOfEndOfData();

  // As this is the end of the data, we do a final buffer build:
  Redrawer::instance().bufferBuild();

  if (Configuration::makeDumpAtEnd) {
    // output to eps if necessary:
    writedump(Configuration::dumpFileName);
  }
  if (Configuration::makeEpsAtEnd) {
    // output to eps if necessary:
    writeeps(Configuration::epsFileName);
  }
  if (Configuration::makePdfAtEnd) {
    // output to pdf if necessary:
    writepdf(Configuration::pdfFileName);
  }
  if (Configuration::makeSvgAtEnd) {
    // output to pdf if necessary:
    writesvg(Configuration::svgFileName);
  }
  if (Configuration::makePngAtEnd) {
    // output to pdf if necessary:
    writepng(Configuration::pngFileName);
  }

  if (Configuration::quitOnEnd) {
    //std::mes() << "Program hist is exiting normally." << std::endl;
    return;
  } else {
    //std::mes() << "Hit ctrl-C to quit the program (or use the 'quit' command line option to avoid needing to press ctrl-C next time you run hist)." << std::endl;
    while(true) {
      sleep(10000);
    }
  }

}


int main(int nArg, char * cArg[]) {

  newHistArgs(nArg, cArg);

  std::shared_ptr<OurXWindow> ourXWindow;

  if (!(Configuration::batch)) {
    ourXWindow=std::shared_ptr<OurXWindow>(new OurXWindow());

    std::thread thrd1(std::bind(&OurXWindow::start, *ourXWindow));
    //std::mes()  << "Waiting for XWindow" << std::endl;
    Synch::waitForXWindowThreadReadyBeforeProceeding();
    //std::mes() << "Finished waiting for XWindow" << std::endl;
    thrd1.detach();
  }

  //std::cout << "Preparing for MainThread" << std::endl;
  std::thread thrd3(mainThread);
  //std::mes() << "Waiting for MainThread" << std::endl;
  Synch::waitForMainThreadReadyBeforeProceeding();
  //std::mes() << "Finished waiting for MainThread" << std::endl;

  if (!(Configuration::batch)) {
    std::thread thrd2(std::bind(&Redrawer::handleRedrawsEtc, &(Redrawer::instance()))); // Don't need for this thread to terminate
    Synch::waitForRedrawerThreadReadyBeforeProceeding();
    //std::mes() << "Waiting for Redrawer" << std::endl;
    thrd2.detach();
  }

  thrd3.join();
  //thrd1.join();
  return 0;
}


