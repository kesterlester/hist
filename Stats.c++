
#include "Stats.h"
#include <cmath>

double Stats::getRMS() const { return sqrt(getVariance()); }
