
#ifndef LESTER_TMPTFMS_H
#define LESTER_TMPTFMS_H

// KLUDGE KLUDGE KLUDGE ... don't use me!

const int SIZEX=500;
const int SIZEY=500;

const double bor=0.1;

inline double tfx(const double x) {
  const double xx=(1-2*bor)*x+bor;
  return xx*SIZEX;
}

inline double tfy(const double y) {
  const double yy=(1-2*bor)*y+bor;
  return (1-yy)*SIZEY;
}

#endif
