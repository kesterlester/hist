
#include "Paintable1DRatioHistogram.h"
#include "TfmHistBox.h"
#include "TfmAxisText.h"
#include "PaintableAxes.h"
#include "PaintableTitle.h"
#include "Configuration.h"
#include "Hist1D.h"
#include "LogScaleHelper.h"
#include <iostream>
#include <cmath> // for std::isfinite( ... )

#include "LogHelper.h"

void Paintable1DRatioHistogram::paint(cairo_t * c, std::ostream & os) const {

  if (m_hist1DPtrMap.empty()) {
    return;
  }

  {
    static bool haveNotYetGivenWarning = true;
    if (m_hist1DPtrMap.size()<2) {
      // can't plot a ratio of any kind, yet ...
      std::cerr << "Waiting for enough data to generate a ratio ..." << std::endl;
      haveNotYetGivenWarning = false;
      return;
    }
  }

  assert(m_hist1DPtrMap.size()>=2);

  Data::Hist1DPtrMap::const_iterator itA = m_hist1DPtrMap.find(Configuration::ratioTypeA);
  Data::Hist1DPtrMap::const_iterator itB = m_hist1DPtrMap.find(Configuration::ratioTypeB);

  {
    static bool haveNotYetGivenWarning = true;
    if (itA==m_hist1DPtrMap.end()||itB==m_hist1DPtrMap.end()) {
      std::cerr << "There are two or more types of data in the input stream, but so far they do not (yet) include [" << Configuration::ratioTypeA<<"] and ["<<Configuration::ratioTypeB<<"] among them.  Waiting for these to arrive." << std::endl;
      haveNotYetGivenWarning = false;
      return;
    }
  }

  assert(itA!=m_hist1DPtrMap.end());
  assert(itB!=m_hist1DPtrMap.end());

  TfmAxisText tat;

  // (1) Set co-ords up to match histogram
  const Hist1D & histA = *(itA->second);
  const Hist1D & histB = *(itB->second);
  const Hist1D & ratioHist = Hist1D::divide(histA,histB);
  double smallestWeightToPlot;
  double largestWeightToPlot;
  findLargestAndSmallestWeightToPlot(true, ratioHist, smallestWeightToPlot, largestWeightToPlot);

  //std::cerr << "SMALLEST " << smallestWeightToPlot << " LARGEST  " << largestWeightToPlot << std::endl;
  TfmHistBox tfm(histA.xAxis.leftMostExtent(),  adapt(smallestWeightToPlot),
                 histA.xAxis.rightMostExtent(), adapt( largestWeightToPlot));

  if (c) tfm.paint(c,os);

  // (2) Fill in the "yellow" of the histogram

  //if (c) cairo_move_to(c, hist.xAxis.from, adapt(smallestWeightToPlot)); // not quite right.  Will be silly if eg liny=true, lx=-2, uy=-1.
  if (!c) {
    ratioHist.dumpTo(os);
  }
  {
    double lastRatio;
    if (c) Configuration::setFGColour(c);
    for (unsigned int n=0; n<histA.xAxis.n; ++n) {

      const Bin & xBin = histA.xAxis.getBinByNumber(n);
      double weightA = histA.weightForBinNumber(n);
      double weightB = histB.weightForBinNumber(n);
      double ratio = weightA/weightB;
      if (weightB==0 || !std::isfinite(ratio)) {
        // FIXME: plot an arrow or something going off the top of the canvas
        const double mid = (xBin.from + xBin.to)*0.5;
        const double left  = (xBin.from + mid)*0.5;
        const double right = (xBin.to + mid)*0.5;
        const double top = adapt(largestWeightToPlot);
        const double arrowFrac = 0.05;
        const double bottom = (1.0-arrowFrac)*adapt(largestWeightToPlot) + arrowFrac*adapt(smallestWeightToPlot);
        const double middle = (top + bottom)*0.5;
        if (c) cairo_move_to(c, left, middle);
        if (c) cairo_line_to(c, mid, top);
        if (c) cairo_line_to(c, right, middle);
        if (c) cairo_move_to(c, mid, top);
        if (c) cairo_line_to(c, mid, bottom);
      } else {
        // ratio is finite and non-zero, but still might disappear off the bottom or top of the screen
        // and ratio might be so close to zero, that when we log it it goes off to minus infinity
        const bool alwaysMove = !Configuration::ratioConnectPoints;

        if (alwaysMove || n==0 || !(std::isfinite(lastRatio) && std::isfinite(ratio))) {
          if (c) cairo_move_to(c, xBin.from, adapt(ratio, smallestWeightToPlot));
        } else {
          if (c) cairo_line_to(c, xBin.from, adapt(ratio, smallestWeightToPlot));
        }
        if (c) cairo_line_to(c, xBin.to  , adapt(ratio, smallestWeightToPlot));
      }
      //if (!c) {
      //  os << xBin.from << " " << xBin.to << " " << ratio << "\n";
      //}
      lastRatio = ratio;
    }
    if (c) tat.paint(c,os);
    if (c) cairo_set_line_width(c,Configuration::graphThickness);
    if (c) cairo_stroke(c);
  }

  /*


  // (3.5) Draw error bars if needed
  if (c) tfm.paint(c,os);
  if (Configuration::drawErrors) {
    if (c) Configuration::setFGColour(c);
    for (unsigned int n=0; n<hist.xAxis.n; ++n) {

      const Bin & xBin = hist.xAxis.getBinByNumber(n);
      double weight = hist.weightForBinNumber(n);


      if (weight>0) {

  //if (c) cairo_set_line_width(c,(-xBin.from+xBin.to)/10);
  if (c) cairo_move_to(c, (xBin.from+xBin.to)*0.5     , adapt(weight+sqrt(fabs(weight)),smallestWeightToPlot   ));
  if (c) cairo_line_to(c, (xBin.from+xBin.to)*0.5 ,     adapt(weight-sqrt(fabs(weight)),smallestWeightToPlot   ));
  if (c) cairo_move_to(c, (xBin.from*.75+xBin.to*.25) , adapt(weight+sqrt(fabs(weight)),smallestWeightToPlot   ));
  if (c) cairo_line_to(c, (xBin.from*.25+xBin.to*.75) , adapt(weight+sqrt(fabs(weight)),smallestWeightToPlot   ));
  if (c) cairo_move_to(c, (xBin.from*.75+xBin.to*.25) , adapt(weight-sqrt(fabs(weight)),smallestWeightToPlot   ));
  if (c) cairo_line_to(c, (xBin.from*.25+xBin.to*.75) , adapt(weight-sqrt(fabs(weight)),smallestWeightToPlot   ));

      }
    }
  }
  if (c) tat.paint(c,os);
  if (c) cairo_set_line_width(c,Configuration::graphThickness);
  if (c) cairo_stroke(c);
  */

  // (4) Now draw axes.
  {
    const Hist1D & aHist = *(m_hist1DPtrMap.begin()->second);
    if (c) tat.paint(c,os);
    PaintableAxes axes(aHist.xAxis, Axis(10,smallestWeightToPlot, largestWeightToPlot, Configuration::haveLogScaleY, false));
    if (c) axes.paint(c,os);
  }

  // FIXME ... THIS SHOULD NOT DUPLICATE CODE IN Paintable1DHistogram.c++ and Paintable2DHistogram.c++ !!!!
  // (5) Now draw title.
  if (Configuration::title != "") {
    PaintableTitle title(Configuration::title);
    if (c) title.paint(c,os);
  }

  if (c) cairo_identity_matrix(c);
  if (!c) {
    os << std::flush;
  }
}


