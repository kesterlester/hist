
#ifndef LESTER_CAIROPAINTABLE_H
#define LESTER_CAIROPAINTABLE_H

#include <cairo/cairo.h>
#include <iostream>

class CairoPaintable {
public:
  virtual void paint(cairo_t * c, std::ostream & os) const = 0; // most things don't need to know how to dump.  Probably only histograms do.  Reason we have this here at all is that the buffer stores cairopaintables ... :(    Hisgorrams should reimplement this method.
  virtual ~CairoPaintable() {};
};

#endif
