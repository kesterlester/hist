
#include "ThePalette.h"
#include "RainbowPalette.h"
#include "AllanachPalette.h"
#include "GreyscalePalette.h"
#include "UserPalette.h"

std::shared_ptr<Palette> ThePalette::get() {
  if (!s_palette) {
    setGreyscale();
  }
  return s_palette;
}

void ThePalette::setRainbow() {
    s_palette = std::shared_ptr<Palette>(new RainbowPalette);
}

void ThePalette::setAllanach() {
    s_palette = std::shared_ptr<Palette>(new AllanachPalette);
}

void ThePalette::setGreyscale() {
    s_palette = std::shared_ptr<Palette>(new GreyscalePalette);
}

void ThePalette::setUser() {
    s_palette = UserPalette::instance();
}


std::shared_ptr<Palette> ThePalette::s_palette;
