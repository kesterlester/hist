
#ifndef LESTER_PROFILEHIST_H
#define LESTER_PROFILEHIST_H

#include "Hist.h"
#include "Axis.h"
#include "Stats2D.h"
#include <map>
#include <string>

class ProfileHist : public Hist {
 public:
  typedef std::pair<Bin, Bin> Bin2D;
 private:
  static const Bin dummyBin;
  const Bin2D getBin(const double x, const double y) const {
     if (Configuration::profile_x && Configuration::profile_y) {
       return Bin2D(dummyBin, dummyBin);
     } else if (Configuration::profile_x) {
       return Bin2D(dummyBin, yAxis.getBin(y));
     } else if (Configuration::profile_y) {
       return Bin2D(xAxis.getBin(x), dummyBin);
     } else {
       std::mes() << "Warning at line " << __LINE__ << " of " << __FILE__ << " : profiling nowhere! Ask Lester to fix an error in the program." << std::endl;
       return Bin2D( xAxis.getBin(x), yAxis.getBin(y) );
     }
  }
 public:
  ProfileHist(Axis xAxis, Axis yAxis) :
    xAxis(xAxis),
    yAxis(yAxis) {
  }
  Axis xAxis;
  Axis yAxis;


  void fill(double x, double y, double weight=1) {
    totWeight += weight;
    const Bin2D & bin = this->getBin(x,y);
    const double w = data[bin].note(x,y,weight).getSumWeight(); // creats a bin entry if it doesn't exist
    updateWeightBounds(w);
  }

  void fillOverwrite(double x, double y, double weight=1) {
    const Bin2D & bin = this->getBin(x,y);
    const double oldWeight = data[bin].getSumWeight();
    data[bin] = Stats2D().note(x,y,weight); // creates bin entry if it doesn't exist
    totWeight += (weight-oldWeight);
    updateWeightBounds(weight);
  }

  bool normalize() {
      return false; // We always fail, since there is no concept of normalizing a profile histogram. It's nonsense.
  }

  
  typedef std::map<Bin2D, Stats2D> Data;
  Data data;

  /*
  // divides histA/histB
  static ProfileHist divide(const ProfileHist & a, const ProfileHist & b) {
    if (a.xAxis != b.xAxis || a.yAxis != b.yAxis) {
      throw std::string("Axes are not the same --- cannot divide one by the other"); // FIXME .. Strictly speaking, could divide histograms if the bins line up -- even if one histogram is not the same shape as the other.  Ought really to replace with a more general divide method that checks for commensurateness.
    }
    ProfileHist ans(a.xAxis, a.yAxis);
    ProfileHist::Data aDataCopy = a.data; // need a copy since we will modify with the trick below:
    // Remember data is SPARSE.
    // Find all B bins, and and then call a[BBib] since it will value-initialise to zero if it doesn't exist -- handy trick.
    for (ProfileHist::Data::const_iterator it = b.data.begin();
         it != b.data.end();
         ++it) {
      const Bin2D & bin = it->first;
      const double & bot = it->second;
      const double top = aDataCopy[bin]; // Magically becomes zero if no data in this bin! :) Trick!
      ans.data[bin] = top/bot;
    }
    // Now we've filled all the non-inf bins ... but technically there are also now "inf" bins where a has data but b doesn't.
    for (ProfileHist::Data::const_iterator it = a.data.begin(); // use a data rather than aDataCopy since a is smaller.
         it!= a.data.end();
         ++it) {
      const Bin2D & bin = it->first;
      const double & top = it->second;
      ProfileHist::Data::const_iterator bit = b.data.find(bin);
      if (bit == b.data.end()) {
        // no b bin exists, so this is an inf:
        ans.data[bin] = top/0.0; // inf or NaN as appropriate
      }
    }
    return ans;
  }
  */

};

# endif





