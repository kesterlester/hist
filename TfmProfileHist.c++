
#include "TfmProfileHist.h"
#include "ProfileHist.h"

TfmProfileHist::TfmProfileHist(const ProfileHist & hist) : m_tfmHistBox(hist.xAxis.leftMostExtent(),
							 hist.yAxis.leftMostExtent(),
							 hist.xAxis.rightMostExtent(),
							 hist.yAxis.rightMostExtent()) {
}

void TfmProfileHist::paint(cairo_t * c, std::ostream & os) const {
  m_tfmHistBox.paint(c,os);
}
