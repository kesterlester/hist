#!/bin/sh
cat EXAMPLES | \
grep -v '^export HIST' | \
gawk '

     /^expo/ { print "<td><img src=",$2,"/></td>" }  
     /^rm/   { print "<td><pre>" ,$0, "</pre></td>" }
     /^$/    { print "</tr><tr>" }

     BEGIN   { 
     print "<html><body><h2>Gallery</h2><p><b>Note:</b> Some of the examples need an input stream of (approximately) normally distributed random numbers.  Here is an example of how you might create a script able to generate such a stream:</p><p><pre>";
     print "cat &lt;&lt; EOF > ./normalDist.sh";
     print "#!/bin/sh";
     print "awk €function gaussian() { tot=0; num=20;for(i=0; i&lt;num; ++i) { tot+=(2.*rand()-1)}; return tot*sqrt(3./num) }  BEGIN{while (1==1) {print gaussian()}}€";
     print "EOF";
     print "chmod +x ./normalDist.sh";
     print "</pre><h2>Examples</h2><table><tr>";
}
     END     { print "</tr></table> <br> <a href='\''index.html'\''>Back to LesterHist homepage</a> </body></html>" }

' | \
sed 's/ FILE=\(.*png\)/"\1"/g' | \
sed 's/ rm -f \$FILE ; //g' | \
sed 's/\$FILE/out.png/g' | \
sed 's/\$HIST mode hist/hist/g' | \
sed 's/ ; display out.png//g' | \
sed 's/ quit//g' | \
tr € \' | \
sed $'s/ | / | \\\DELETEME\\\n/g' | \
sed 's/DELETEME//g' | \
cat > examplesFragment.html

#scp *.png web/*.png index.html examplesFragment.html pclv.hep.phy.cam.ac.uk:/usera/lester/public_html/hist/
#ssh pclv.hep.phy.cam.ac.uk chmod a+r /usera/lester/public_html/hist/*.png
