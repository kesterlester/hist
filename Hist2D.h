
#ifndef LESTER_HIST2D_H
#define LESTER_HIST2D_H

#include "Hist.h"
#include "Axis.h"
#include <map>
#include <string>

class Hist2D : public Hist {
 public:
  Hist2D(Axis xAxis, Axis yAxis) :
    xAxis(xAxis),
    yAxis(yAxis) {
  }
  Axis xAxis;
  Axis yAxis;

  void fill(double x, double y, double weight=1) {
    totWeight += weight;
    const Bin2D & bin = Bin2D( xAxis.getBin(x), yAxis.getBin(y) );
    const double w = (data[bin] += weight); // creates bin entry if it doesn't exist
    updateWeightBounds(w);
  }

  void fillOverwrite(double x, double y, double weight=1) {
    const Bin2D & bin = Bin2D( xAxis.getBin(x), yAxis.getBin(y) );
    const double oldWeight = data[bin];
    data[bin] = weight; // creates bin entry if it doesn't exist
    totWeight += (weight-oldWeight);
    updateWeightBounds(weight);
  }

  typedef std::pair<Bin, Bin> Bin2D;

  bool normalize() {
    if (totWeight!=0) {
      for (Data::iterator it = data.begin();
           it!=data.end();
           ++it) {
        (*it).second /= totWeight;
      }
      finaliseNormalize();
      return true; // We succeeded
    } else {
      return false; // we could not normalie
    }
  }

  typedef std::map<Bin2D, double> Data;
  Data data;

  // divides histA/histB
  static Hist2D divide(const Hist2D & a, const Hist2D & b) {
    if (a.xAxis != b.xAxis || a.yAxis != b.yAxis) {
      throw std::string("Axes are not the same --- cannot divide one by the other"); // FIXME .. Strictly speaking, could divide histograms if the bins line up -- even if one histogram is not the same shape as the other.  Ought really to replace with a more general divide method that checks for commensurateness.
    }
    Hist2D ans(a.xAxis, a.yAxis);
    Hist2D::Data aDataCopy = a.data; // need a copy since we will modify with the trick below:
    // Remember data is SPARSE.
    // Find all B bins, and and then call a[BBib] since it will value-initialise to zero if it doesn't exist -- handy trick.
    for (Hist2D::Data::const_iterator it = b.data.begin();
         it != b.data.end();
         ++it) {
      const Bin2D & bin = it->first;
      const double & bot = it->second;
      const double top = aDataCopy[bin]; // Magically becomes zero if no data in this bin! :) Trick!
      ans.data[bin] = top/bot;
    }
    // Now we've filled all the non-inf bins ... but technically there are also now "inf" bins where a has data but b doesn't.
    for (Hist2D::Data::const_iterator it = a.data.begin(); // use a data rather than aDataCopy since a is smaller.
         it!= a.data.end();
         ++it) {
      const Bin2D & bin = it->first;
      const double & top = it->second;
      Hist2D::Data::const_iterator bit = b.data.find(bin);
      if (bit == b.data.end()) {
        // no b bin exists, so this is an inf:
        ans.data[bin] = top/0.0; // inf or NaN as appropriate
      }
    }
    return ans;
  }

};

# endif





