#ifndef LESTER_OURXWINDOW_H
#define LESTER_OURXWINDOW_H

#include "XWindow.h"

struct OurXWindow : public XWindow {
  void paint(cairo_t * cs, std::ostream & os) const;
};

#endif
