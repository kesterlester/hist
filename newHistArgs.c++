
#include "newHistArgs.h"

#include "Configuration.h"
#include "ThePalette.h"
#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <map>
#include "stringFrom.h"
#include <cassert>
#include "UserPalette.h"
#include <algorithm>

#define url_unescape( X ) X


void usage(const std::string & builtPart);

void processSilent(const std::vector<std::string> & args) {
  std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), "silent");
  if (namePtr!=args.end() && !Configuration::showHelp) {
    Configuration::silent=true;
  }
}

void newHistArgs(int nArg, char * cArg[]) {

  std::string cmd;
  std::map<std::string, std::string> help;

  std::vector<std::string> args;
  for (int i=1; i<nArg; i++) {
    const std::string arg=cArg[i];
    args.push_back(arg);
  };


  // Has to be processed before the other args:

  std::string ourName = (nArg>=1  ? cArg[0] : "newHist" );
  {
    int pos;
    while ((pos=ourName.find("/")) != std::string::npos) {
      ourName = ourName.substr(pos+1);
    }
  }
  const std::string smallIndent = "   ";
  const std::string indent      = "             ";

  std::ostringstream os; // For usage message
  os << "Usage:\n\n";
  os << smallIndent << ourName << " [help [command]]\n\n";
  os << "or\n\n";
  os << smallIndent << ourName;

  bool mustStop = false;
  bool & showHelp = Configuration::showHelp;
  std::string showHelpCmd = "";

  {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), "help");
    if (namePtr!=args.end()) {
      showHelp = true;
      if (namePtr!=args.end() && namePtr+1 !=args.end()) {
        showHelpCmd=(*(namePtr+1));
      }
    } else {
      // We are going to be friendly:
      std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), "--help");
      if (namePtr!=args.end()) {
        showHelp = true;
        if (namePtr!=args.end() && namePtr+1 !=args.end()) {
          showHelpCmd=(*(namePtr+1));
        }
      } else {
        // We are going to be very friendly:
        std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), "-help");
        if (namePtr!=args.end()) {
          showHelp = true;
          if (namePtr!=args.end() && namePtr+1 !=args.end()) {
            showHelpCmd=(*(namePtr+1));
          }
        }
      }

    }
  }

  // Don't process silent command if user requested help, otherwise do it early:
  if (!showHelp) {
    processSilent(args);
  }

  //std::ostream & out = showHelp ? oblivion : std::mes();
  //std::ostream & err = showHelp ? oblivion : std::cerr;
  std::ostream & err = std::mes();

  // FIRST DETERMINE MODE !!!
  {
    os << " [mode hist|hist2D|histWeighted|hist2DWeighted]\n";
    bool set_mode = false;
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), "mode");
    if (namePtr!=args.end() && (namePtr+1)!=args.end()) {
      Configuration::fromName((*(namePtr+1)), err);
      set_mode = true;
    }

    if (!set_mode) {
      Configuration::fromName("not-specified", err); // makes a default choice
      set_mode=true;
    }

    assert(set_mode);
  }

  // NOW MODE HAS BEEN DETERMINED !!

  os << indent << "[n N]";
  if (Configuration::dim>=1) {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), "n");
    if (namePtr!=args.end() && namePtr+1 !=args.end()) {
      Configuration::calcnx=false;
      Configuration::nx=stringTo<int>(*(namePtr+1));
      std::mes() << "Set number of xbins: "<<Configuration::nx <<std::endl;
      if (Configuration::dim>=2) {
        Configuration::calcny=false;
        Configuration::ny=stringTo<int>(*(namePtr+1));
        std::mes() << "Set number of ybins: "<<Configuration::ny <<std::endl;
      }
    }
  }
  os << " [l N]";
  if (Configuration::dim>=1) {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), "l");
    if (namePtr!=args.end() && namePtr+1 !=args.end()) {
      Configuration::calclx=false;
      Configuration::lx=stringTo<double>(*(namePtr+1));
      std::mes() << "Set lower x bound: "<< Configuration::lx <<std::endl;
      if (Configuration::dim>=2) {
        Configuration::calcly=false;
        Configuration::ly=stringTo<double>(*(namePtr+1));
        std::mes() << "Set lower y bound: "<< Configuration::ly <<std::endl;
      }
    }
  }
  os << " [u N]";
  if (Configuration::dim>=1) {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), "u");
    if (namePtr!=args.end() && namePtr+1 !=args.end()) {
      Configuration::calcux=false;
      Configuration::ux=stringTo<double>(*(namePtr+1));
      std::mes() << "Set upper x bound: "<<Configuration::ux <<std::endl;
      if (Configuration::dim>=2) {
        Configuration::calcuy=false;
        Configuration::uy=stringTo<double>(*(namePtr+1));
        std::mes() << "Set upper y bound: "<<Configuration::uy <<std::endl;
      }
    }
  }
  cmd = "int";
  os << " ["<<cmd<<"]\n";
  if (Configuration::dim>=1) {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), cmd);
    if (namePtr!=args.end()) {
      Configuration::calcnx=false;
      Configuration::intx=true;
      std::mes() << "Set integer x axis."<<std::endl;
      if (Configuration::dim>=2) {
      Configuration::calcny=false;
        Configuration::inty=true;
        std::mes() << "Set integer y axis."<<std::endl;
      }
    }
  }
  os << indent << "[nx N]";
  if (Configuration::dim>=2) {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), "nx");
    if (namePtr!=args.end() && namePtr+1 !=args.end()) {
      Configuration::calcnx=false;
      Configuration::nx=stringTo<int>(*(namePtr+1));
      std::mes() << "Set number of xbins: "<<Configuration::nx <<std::endl;
    }
  }
  os << " [lx N]";
  if (Configuration::dim>=1) {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), "lx");
    if (namePtr!=args.end() && namePtr+1 !=args.end()) {
      Configuration::calclx=false;
      Configuration::lx=stringTo<double>(*(namePtr+1));
      std::mes() << "Set lower x bound: "<<Configuration::lx <<std::endl;
    }
  }
  os << " [ux N]";
  if (Configuration::dim>=1) {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), "ux");
    if (namePtr!=args.end() && namePtr+1 !=args.end()) {
      Configuration::calcux=false;
      Configuration::ux=stringTo<double>(*(namePtr+1));
      std::mes() << "Set upper x bound: "<<Configuration::ux <<std::endl;
    }
  }
  cmd = "intx";
  os << " ["<<cmd<<"]";
  if (Configuration::dim>=1) {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), cmd);
    if (namePtr!=args.end()) {
      Configuration::calcnx=false;
      Configuration::intx=true;
      std::mes() << "Set integer x axis."<<std::endl;
    }
  }
  os << " [ny N]";
  if (Configuration::dim>=2) {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), "ny");
    if (namePtr!=args.end() && namePtr+1 !=args.end()) {
      Configuration::calcny=false;
      Configuration::ny=stringTo<int>(*(namePtr+1));
      std::mes() << "Set number of ybins: "<<Configuration::ny <<std::endl;
    }
  }
  os << " [ly N]";
  // 1d ratio histos use ly ...   if (Configuration::dim>=2)
  {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), "ly");
    if (namePtr!=args.end() && namePtr+1 !=args.end()) {
      Configuration::calcly=false;
      Configuration::ly=stringTo<double>(*(namePtr+1));
      std::mes() << "Set lower y bound: "<<Configuration::ly <<std::endl;
    }
  }
  os << " [uy N]";
  // 1d ratio histos use ly ...   if (Configuration::dim>=2)
  {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), "uy");
    if (namePtr!=args.end() && namePtr+1 !=args.end()) {
      Configuration::calcuy=false;
      Configuration::uy=stringTo<double>(*(namePtr+1));
      std::mes() << "Set upper y bound: "<<Configuration::uy <<std::endl;
    }
  }
  cmd = "inty";
  os << " ["<<cmd<<"]";
  if (Configuration::dim>=2) {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), cmd);
    if (namePtr!=args.end()) {
      Configuration::calcny=false;
      Configuration::inty=true;
      std::mes() << "Set integer y axis."<<std::endl;
    }
  }
  os << " [lz N]";
  if (Configuration::dim>=1) {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), "lz");
    if (namePtr!=args.end() && namePtr+1 !=args.end()) {
      Configuration::calclz=false;
      Configuration::lz=stringTo<double>(*(namePtr+1));
      std::mes() << "Set lower z bound: "<<Configuration::lz <<std::endl;
    }
  }
  os << " [uz N]\n";
  if (Configuration::dim>=2) {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), "uz");
    if (namePtr!=args.end() && namePtr+1 !=args.end()) {
      Configuration::calcuz=false;
      Configuration::uz=stringTo<double>(*(namePtr+1));
      std::mes() << "Set upper z bound: "<<Configuration::uz <<std::endl;
    }
  }
  os << indent << "[title TITLE]";
  {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), "title");
    if (namePtr!=args.end() && namePtr+1 !=args.end()) {
      //Configuration::nitle=false;
      Configuration::title=(*(namePtr+1));
      Configuration::title=url_unescape(Configuration::title);
      std::mes() << "Histogram titled [" << Configuration::title<< "]" <<std::endl;
    }
  }
  os << " [notitle]";
  {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), "notitle");
    if (namePtr!=args.end()) {
      Configuration::title="";
      std::mes() << "Will not display title"<<std::endl;
    }
  }
  os << " [xname NAME]";
  if (true) {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), "xname");
    if (namePtr!=args.end() && namePtr+1 !=args.end()) {
      //Configuration::havexname=true;
      Configuration::xname=(*(namePtr+1));
      Configuration::xname=url_unescape(Configuration::xname);
      std::mes() << "X axis named [" << Configuration::xname << "]" <<std::endl;
    }
  }
  os << " [yname NAME]\n";
  if (true) {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), "yname");
    if (namePtr!=args.end() && namePtr+1 !=args.end()) {
      //Configuration::haveyname=true;
      Configuration::yname=(*(namePtr+1));
      Configuration::yname=url_unescape(Configuration::yname);
      std::mes() << "Y axis named [" << Configuration::yname << "]" <<std::endl;
    }
  }
  os << indent << "[rightjustify]";
  if (true) {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), "rightjustify");
    if (namePtr!=args.end()) {
      Configuration::centreJustify=false;
      std::mes() << "Will right justify axis titles" <<std::endl;
    }
  }
  os << " [centrejustify]";
  if (true) {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), "centrejustify");
    if (namePtr!=args.end()) {
      Configuration::centreJustify=true;
      std::mes() << "Will centre justify axis titles" <<std::endl;
    }
  }
  os << " [ynamedirection UP|DOWN|NORMAL]\n";
  if (true) {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), "ynamedirection");
    if (namePtr!=args.end()) {
      if ((namePtr+1)!=args.end()) {
        const std::string direc = *(namePtr+1);
        if (direc=="UP") {
          Configuration::yAxisLabelDirection = Configuration::UP;
          std::mes() << "Will draw yLabels with text running upwards." <<std::endl;
        } else if (direc=="DOWN") {
          Configuration::yAxisLabelDirection = Configuration::DOWN;
          std::mes() << "Will draw yLabels with text running downwards." <<std::endl;
        } else if (direc=="NORMAL") {
          Configuration::yAxisLabelDirection = Configuration::NORMAL;
          std::mes() << "Will draw yLabels with text running horizontally." <<std::endl;
        } else {
          std::mes() << "I don't recognise the direction you wanted your yAxis text to go in .. you requested [" << direc << "] but you must choose one of UP or DOWN or NORMAL." << std::endl;
        }
      } else {
        std::mes() << "You must follow the keyword 'ynamedirection' with one of UP or DOWN or NORMAL." << std::endl;
        mustStop = true;
      }
    }
  }
  /*
  if (Configuration::dim==1) {
    style = style1DDefault;
  } else {
    style = style2DDefault;
  }
  */
  os << indent << "[logy]";
  if (Configuration::dim==1) {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), "logy");
    if (namePtr!=args.end()) {
      Configuration::haveLogScaleY=true;
      std::mes() << "Set log scale on y axis"<<std::endl;
    }
  }
  os << " [logz]\n";
  if (Configuration::dim==2) {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), "logz");
    if (namePtr!=args.end()) {
      Configuration::haveLogScaleZ=true;
      std::mes() << "Set log scale on z axis"<<std::endl;
    }
  }

  os << indent << "[quit]";
  help["quit"] = "The 'quit' command is usually used in batch mode or scripts that are being viewed in real time by humans.  It asks hist to quit as soon as all data has been read, and any saving operations have been performed (e.g. exports to pdf, svg or png).  See also 'batch', which imples 'quit', but disables the XWindow -- sensible for unattended runnning.";
  if (true) {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), "quit");
    if (namePtr!=args.end()) {
      Configuration::quitOnEnd=true;
      std::mes() << "Will quit when input ends." <<std::endl;
    };
  }
  os << " [showargs]\n";
  if (true) {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), "showargs");
    if (namePtr!=args.end()) {
      std::mes() << "HISTARGS " << cArg[0] << " ";
      for (std::vector<std::string>::const_iterator it = args.begin();
           it != args.end();
           ++it)  {
        if(it!=args.end()) {
          std::mes() << " ";
        }
        std::mes() << *it;
      }
      std::mes() << "\n";
    }
  }
  cmd = "eps";
  os << indent << "["<<cmd<<" FILENAME]";
  help[cmd]=std::string("'hist ")+cmd+" FILENAME.eps' waits until all the input data has been read, and then saves a snapshot of the final displayed histogram as an Encapsulated Postscript file named FILENAME.eps.  In a script, you probably want to use this in conjunction with the 'quit' command.";
  if (true) {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), "eps");
    if (namePtr!=args.end() && namePtr+1 ==args.end()) {
      std::mes() << "Error: '"<<cmd<<"' not followed by output filename on command line." << std::endl;
      mustStop = true;
    }
    if (namePtr!=args.end() && namePtr+1 !=args.end()) {
      Configuration::makeEpsAtEnd=true;
      Configuration::epsFileName=(*(namePtr+1));
      std::mes() << "Will prduce eps file at end with name: ["<< Configuration::epsFileName <<"]"<<std::endl;
    }
  }
  cmd = "pdf";
  os << " ["<<cmd<<" FILENAME]";
  help[cmd]=std::string("'hist ")+cmd+" FILENAME' waits until all the input data has been read, and then saves a snapshot of the final displayed histogram as a Portable Document Format file named FILENAME.  In a script, you probably want to use this in conjunction with the 'quit' command.";
  if (true) {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), cmd);
    if (namePtr!=args.end() && namePtr+1 ==args.end()) {
      std::mes() << "Error: '"<<cmd<<"' not followed by output filename on command line." << std::endl;
      mustStop = true;
    }
    if (namePtr!=args.end() && namePtr+1 !=args.end()) {
      Configuration::makePdfAtEnd=true;
      Configuration::pdfFileName=(*(namePtr+1));
      std::mes() << "Will produce pdf file at end with name: ["<< Configuration::pdfFileName <<"]"<<std::endl;
    }
  }
  cmd = "svg";
  os << " ["<<cmd<<" FILENAME]";
  help[cmd]=std::string("'hist ")+cmd+" FILENAME' waits until all the input data has been read, and then saves a snapshot of the final displayed histogram as a Scalable Vector Graphic image file named FILENAME.  In a script, you probably want to use this in conjunction with the 'quit' command.";
  if (true) {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), cmd);
    if (namePtr!=args.end() && namePtr+1 ==args.end()) {
      std::mes() << "Error: '"<<cmd<<"' not followed by output filename on command line." << std::endl;
      mustStop = true;
    }
    if (namePtr!=args.end() && namePtr+1 !=args.end()) {
      Configuration::makeSvgAtEnd=true;
      Configuration::svgFileName=(*(namePtr+1));
      std::mes() << "Will produce svg file at end with name: ["<< Configuration::svgFileName <<"]"<<std::endl;
    }
  }
  cmd = "png";
  os << " ["<<cmd<<" FILENAME]";
  help[cmd]=std::string("'hist ")+cmd+" FILENAME' waits until all the input data has been read, and then saves a snapshot of the final displayed histogram as a Portable Network Graphic image file named FILENAME.  In a script, you probably want to use this in conjunction with the 'quit' command.";
  if (true) {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), cmd);
    if (namePtr!=args.end() && namePtr+1 ==args.end()) {
      std::mes() << "Error: '"<<cmd<<"' not followed by output filename on command line." << std::endl;
      mustStop = true;
    }
    if (namePtr!=args.end() && namePtr+1 !=args.end()) {
      Configuration::makePngAtEnd=true;
      Configuration::pngFileName=(*(namePtr+1));
      std::mes() << "Will produce png file at end with name: ["<< Configuration::pngFileName <<"]"<<std::endl;
    }
  }
  cmd = "dump";
  os << " ["<<cmd<<" FILENAME]\n";
  help[cmd]=std::string("'hist ")+cmd+" FILENAME' waits until all the input data has been read, and then saves the bin positions and bin-content of the displayed histograms to an ascii plain-text file named FILENAME.  In a script, you probably want to use this in conjunction with the 'quit' command.";
  if (true)   {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), cmd);
    if (namePtr!=args.end() && namePtr+1 ==args.end()) {
      std::mes() << "Error: '"<<cmd<<"' not followed by output filename on command line." << std::endl;
      mustStop = true;
    }
    if (namePtr!=args.end() && namePtr+1 !=args.end()) {
      Configuration::makeDumpAtEnd=true;
      Configuration::dumpFileName=(*(namePtr+1));
      std::mes() << "Will produce dump of histogram bin content at end to file with name: ["<< Configuration::dumpFileName <<"]"<<std::endl;
    }
  }
  os << indent << "[nobox]";
  if (true) {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), "nobox");
    if (namePtr!=args.end()) {
      Configuration::noBox=true;
      std::mes() << "Will not display statistics box"<<std::endl;
    }
  }
  os << " [box]";
  if (true) {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), "box");
    if (namePtr!=args.end()) {
      Configuration::noBox=false;
      std::mes() << "Will display statistics box"<<std::endl;
    }
  }
  os << " [boxx N]";
  if (true) {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), "boxx");
    if (namePtr!=args.end() && namePtr+1 !=args.end()) {
      Configuration::haveBoxX=true;
      Configuration::boxx=stringTo<double>(*(namePtr+1));
      std::mes() << "Set statistics box x to "<<Configuration::boxx <<std::endl;
    }
  }
  os << " [boxy N]\n";
  if (true) {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), "boxy");
    if (namePtr!=args.end() && namePtr+1 !=args.end()) {
      Configuration::haveBoxY=true;
      Configuration::boxy=stringTo<double>(*(namePtr+1));
      std::mes() << "Set statistics box y to "<<Configuration::boxy <<std::endl;
    }
  }
  os << indent << "[mark]";
  if (Configuration::dim==2) {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), "mark");
    if (namePtr!=args.end()) {
      Configuration::markLastPoint=true;
      std::mes() << "Will mark last point." << std::endl;
    };
  }
  os << " [overwrite]";
  if (true || Configuration::dim==2) {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), "overwrite");
    if (namePtr!=args.end()) {
      Configuration::overwrite=true;
      std::mes() << "Will overwrite data rather than filling it." <<std::endl;
    };
  }
  os << " [ignoreweights]";
  if (true || Configuration::dim==2) {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), "ignoreweights");
    if (namePtr!=args.end()) {
      Configuration::ignoreweights=true;
      std::mes() << "Will treat all data as equally weighted, regardless of weight." <<std::endl;
    };
  }
  os << " [style STYLE]";
  if (true) {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), "style");
    if (namePtr!=args.end() && (namePtr+1)!=args.end()) {
      Configuration::style=(*(namePtr+1));
      std::mes() << "Set histogram style: [" << Configuration::style << "]" <<std::endl;
    }
  }
  os << " [errors]\n";
  if (true) {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), "errors");
    if (namePtr!=args.end()) {
      Configuration::drawErrors=true;
      std::mes() << "Will draw simple (sqrt(n)) errors"<<std::endl;
    }
  }
  os << indent << "[rainbow]";
  if (Configuration::dim==2) {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), "rainbow");
    if (namePtr!=args.end()) {
      ThePalette::setRainbow();
      std::mes() << "Using rainbow palette."<<std::endl;
    }
  }
  os << " [allanach]";
  if (Configuration::dim==2) {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), "allanach");
    if (namePtr!=args.end()) {
      ThePalette::setAllanach();
      std::mes() << "Using allanach palette."<<std::endl;
    }
  }
  os << " [greyscale]";
  if (Configuration::dim==2) {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), "greyscale");
    if (namePtr!=args.end()) {
      ThePalette::setGreyscale();
      std::mes() << "Using greyscale palette."<<std::endl;
    }
  }
  os << " [userpal W R G B]";
  if (Configuration::dim==2) {
    bool keepGoing = true;
    while (keepGoing) {
      keepGoing=false;
      std::vector<std::string>::iterator namePtr=find(args.begin(),args.end(), "userpal");
      if (namePtr != args.end()) {
        keepGoing=true;
        const int pos = namePtr - args.begin();
        const int argsLeftIncludingThisOne = args.size() - pos;
        const int expectedWords = 5;  // 5 being the count of words "userpal" "W" "R" "G" "B"
        if (argsLeftIncludingThisOne>=expectedWords) {
          const double interpolationPoint = stringTo<double>(*(namePtr+1));
          const Colour colour(stringTo<double>(*(namePtr+2)),
                              stringTo<double>(*(namePtr+3)),
                              stringTo<double>(*(namePtr+4)));
          ThePalette::setUser();
          UserPalette::instance()->setInterpolant(interpolationPoint, colour);
          std::mes() << "Added user palette interpolant: weight " << interpolationPoint << " colour " << colour << std::endl;
        } else {
          std::mes() << "Wrong number of arguments after 'userpal' ... expected 4: a weight followed by a red a green and a blue value, the latter three being between 0 and 1." << std::endl;
          mustStop = true;
        }
        args.erase(namePtr, namePtr+(argsLeftIncludingThisOne < expectedWords ? argsLeftIncludingThisOne : expectedWords));
      }
    }
  }
  os << " [colourscale]";
  if (Configuration::dim==2) {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), "colourscale");
    if (namePtr!=args.end()) {
      Configuration::colourScale = true;
      std::mes() << "Will draw a colour scale."<<std::endl;
    }
  }
  os << " [noautoscalezcolours]";
  if (Configuration::dim==2) {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), "noautoscalezcolours");
    if (namePtr!=args.end()) {
      Configuration::scaleWeightsToUnitIntervalWhenConvertingToColours = false;
      std::mes() << "Will NOT scale weights in coloured 2D plots so that 0 maps to 0 and maxWeight maps to 1 (which is the default)."<<std::endl;
    }
  }
  os << " [fillvoid]";
  if (Configuration::dim==2) {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), "fillvoid");
    if (namePtr!=args.end()) {
      Configuration::fillVoid = true;
      std::mes() << "Will fill void areas of 2D histogram with colour appropriate for weight 0."<<std::endl;
    }
  }
  os << " [nofillvoid]";
  if (Configuration::dim==2) {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), "nofillvoid");
    if (namePtr!=args.end()) {
      Configuration::fillVoid = false;
      std::mes() << "Will not fill void areas of 2D histograms."<<std::endl;
    }
  }
  os << " [dark]\n";
  help["dark"] = "If enabled, make a plot with white foreground text & axes with a black background. Defaults to disabled, whereby a black foreground is used on a white background.";
  if (true) {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), "dark");
    if (namePtr!=args.end()) {
      Configuration::dark=true;
      std::mes() << "Will use light on dark background" <<std::endl;
    }
  }
  cmd = "multi";
  os << indent << "["<<cmd<<"]";
  help[cmd]=std::string("The '")+cmd+"' command is used to histogram more than one category of data on a single histogram.\nIt requires each input event in the input stream to be preceded by a 'label' defining its type.  For example, to histogram ages for boys and girls one might need an input stream like this:\n\ncat << EOF | hist multi n 10 l 0 u 10 xname 'Age (years)' yname 'frequency (children)' title 'Boys and Girls ages'\n"+
            "boy  4\n"+
            "boy  2\n"+
            "girl 8\n"+
            "girl 9\n"+
            "girl 8\n"+
            "girl 6\n"+
            "boy  3\n"+
            "boy  3\n"+
            "girl 7\n"+
            "girl 6\n"+
            "boy  4\n"+
            "girl 8\n"+
            "boy  3\n"+
            "girl 9\n"+
            "boy  2\n"+
            "EOF"
            ;
  if (Configuration::dim==1 || Configuration::dim==2) {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), cmd);
    if (namePtr!=args.end()) {
      Configuration::multi = true;
      std::mes() << "Prefix each data value with a `type', eg 'sig', 'bg', 'cars' etc ....."<<std::endl;
    }
  }
  cmd = "ratio";
  os << " ["<<cmd<<" A B]";
  help[cmd]=std::string("The '")+cmd+"' command is used to plot a ratio of two histograms -- that containing data of type A divided by that containing data of type B.  Implies 'multi'.  Users using this option myst also define 'uy' and 'ly' (1D) or 'uz' and 'lz' (2D) appropriately, as there is not (yet) any auto-ranging for the ratio-axis.  The ratio command isIgnored if not used in 1D or 2D hisogramming mode.  No histograms will be plotted until both types of data (A and B) are found in the input stream.";
  {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), cmd);
    if (namePtr!=args.end()) {
      // user specifies ratio
      if (!(Configuration::dim==1 || Configuration::dim==2)) {
        std::mes() << "Can only use ratio plot in 1D and 2D mode" << std::endl;
      } else {
        if (!(namePtr+1 != args.end() && namePtr+2 != args.end())) {
          std::mes() << "Must supply names of two data-types to the ratio command." << std::endl;
        } else {
          Configuration::ratio=true;
          Configuration::multi=true;
          Configuration::ratioTypeA = *(namePtr+1);
          Configuration::ratioTypeB = *(namePtr+2);
        }
      }
    }
  }
  cmd = "ratio_connect_points";
  os << " [" << cmd<<"]";
  help[cmd]=std::string("The '")+cmd+"' command connects points in ratio histograms. See also 'ratio_disconnect_points'.";
  {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), cmd);
    if (namePtr!=args.end()) {
      Configuration::ratioConnectPoints = true;
    }
  }
  cmd = "ratio_disconnect_points";
  os << " [" << cmd<<"]";
  help[cmd]=std::string("The '")+cmd+"' command disconnects points in ratio histograms. See also 'ratio_connect_points'.";
  {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), cmd);
    if (namePtr!=args.end()) {
      Configuration::ratioConnectPoints = false;
    }
  }
  os << "\n";
  cmd = "profile";
  os << indent << "[" << cmd<<" x|y|xy]";
  help[cmd]=std::string("The '")+cmd+"' command (which can only be used in 2D mode) cases the data in any bin to be represented by box, centred on the mean (x,y) value in that box, and with a width of (2*SD(x), 2*SD(y)) in each direction..";
  {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), cmd);
    if (namePtr!=args.end()) {

            bool fail = false;
            if (namePtr+1 ==args.end()) { 
                    fail = true;
            }
            std::string profile_type;
            if (!fail) {
                    profile_type = (*(namePtr+1));
                    if ( !(profile_type == "x" || profile_type=="y" || profile_type=="xy")) {
                	    fail = true;
                    }
            }

            if (fail) {
                    std::mes() << "The '" << cmd << "' command should be followed by one of 'x', 'y' or 'xy'." << std::endl;
            }

            if (Configuration::dim!=2) { 
                    std::mes() << "The '" << cmd << "' command should only be used in 2D mode." << std::endl;
                    fail = true;
            }

            if (!fail) {
                    if (profile_type=="x") {
                	    Configuration::profile_x = true;
                	    Configuration::crop_x = false; // Can be overriden by user
			    if (Configuration::calcnx) {
				    // User has not (yet) specified nx
                                    std::mes() << "Setting nx to 1" << std::endl; 
				    Configuration::calcnx=false;
				    Configuration::nx=1;
			    }
                            std::mes() << "Creating a histogram which profiles over x in bins of y only." << std::endl; 
                    }
                    if (profile_type=="y") {
                	    Configuration::profile_y = true;
                	    Configuration::crop_y = false; // Can be overriden by user
			    if (Configuration::calcny) {
				    // User has not (yet) specified ny
                                    std::mes() << "Setting ny to 1" << std::endl; 
				    Configuration::calcny=false;
				    Configuration::ny=1;
			    }
                            std::mes() << "Creating a histogram which profiles over y in bins of x only." << std::endl; 
                    }
                    if (profile_type=="xy") {
                	    Configuration::profile_x = true;
                	    Configuration::profile_y = true;
                	    Configuration::crop_x = false; // Can be overriden by user
                	    Configuration::crop_y = false; // Can be overriden by user
			    if (Configuration::calcnx) {
				    // User has not (yet) specified nx
                                    std::mes() << "Setting nx to 1" << std::endl; 
				    Configuration::calcnx=false;
				    Configuration::nx=1;
			    }
			    if (Configuration::calcny) {
				    // User has not (yet) specified ny
                                    std::mes() << "Setting ny to 1" << std::endl; 
				    Configuration::calcny=false;
				    Configuration::ny=1;
			    }
                            std::mes() << "Creating a histogram which profiles in bins of x and y." << std::endl; 
                    }
            }
    }
  }  
  cmd = "fadeprofile";
  os << " [" << cmd<<"]";
  help[cmd]=std::string("The '")+cmd+"' command has no effect unless the 'profile' command is also used.  It causes the profile box to be drawn dark in bins with high weight, and faded (even invisible) in bins of low (or zero) weight.";
  {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), cmd);
    if (namePtr!=args.end()) {
	    if (Configuration::dim==2) {
		    std::mes() << "If the 'profile' command has been used, the profile histogram will be faded based on weight per bin." << std::endl; 
                    Configuration::fadeProfileUsingWeight = true;
            } else {
		    std::mes() << "Ignoring ["<<cmd<<"] command as hist not in 2D mode." << std::endl; 
	    }
    }
  }
  os << " [[no]cropx] [[no]cropy]";
  cmd = "cropx";
  help[cmd] = std::string("By default, data that falls outside a histogram's bounds is discarded (except for the purposes of mean and RMS stats) in all histograms except profile histograms which usually need to retain such data.  Such behaviour can be overriden with the '(no)cropx' and '(no)cropy' commands -- e.g. if you do want the profiled data to be cropped, or want histograms to bleed outside their limits.");
  {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), cmd);
    if (namePtr!=args.end()) {
      Configuration::crop_x = true;
    }
  }
  cmd = "cropy";
  help[cmd] = std::string("By default, data that falls outside a histogram's bounds is discarded (except for the purposes of mean and RMS stats) in all histograms except profile histograms which usually need to retain such data.  Such behaviour can be overriden with the '(no)cropx' and '(no)cropy' commands -- e.g. if you do want the profiled data to be cropped, or want histograms to bleed outside their limits.");
  {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), cmd);
    if (namePtr!=args.end()) {
      Configuration::crop_y = true;
    }
  }
  cmd = "nocropx";
  help[cmd] = std::string("By default, data that falls outside a histogram's bounds is discarded (except for the purposes of mean and RMS stats) in all histograms except profile histograms which usually need to retain such data.  Such behaviour can be overriden with the '(no)cropx' and '(no)cropy' commands -- e.g. if you do want the profiled data to be cropped, or want histograms to bleed outside their limits.");
  {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), cmd);
    if (namePtr!=args.end()) {
      Configuration::crop_x = false;
    }
  }
  cmd = "nocropy";
  help[cmd] = std::string("By default, data that falls outside a histogram's bounds is discarded (except for the purposes of mean and RMS stats) in all histograms except profile histograms which usually need to retain such data.  Such behaviour can be overriden with the '(no)cropx' and '(no)cropy' commands -- e.g. if you do want the profiled data to be cropped, or want histograms to bleed outside their limits.");
  {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), cmd);
    if (namePtr!=args.end()) {
      Configuration::crop_y = false;
    }
  }
  os << "\n";
  cmd = "nolegend";
  os << indent << "[" << cmd<<"]\n";
  help[cmd]=std::string("The '")+cmd+"' command prevents display of a legend in multi mode.";
  {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), cmd);
    if (namePtr!=args.end()) {
      Configuration::showLegend = false;
    }
  }
  cmd = "normalize";
  os << indent << "[" << cmd << "]\n";
  help[cmd] = std::string("The '")+cmd+"' command requests that any histograms generated from the input stream are (re) normalised such that the sum of their weights is unity just before they are plotted.  If used with the 'ratio' command, then it is the histograms use to MAKE the ratio which are normalised, not the quotient that results from them.";
  {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), cmd);
    if (namePtr!=args.end()) {
      Configuration::normalize = true;
    }
  }
  cmd = "cumulate";
  os << indent << "[" << cmd << "]\n";
  help[cmd] = std::string("The '")+cmd+"' command only affects 1D histograms. Internally these histograms are created in the manner  one would expect from all the other arguments on the command line. However, when drawn, each bin is assigned the sum of itself and all the bins to its left.";
  {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), cmd);
    if (namePtr!=args.end()) {
      Configuration::cumulate = true;
    }
  }
  cmd = "silent";
  os << indent <<"[" << cmd << "]";
  help[cmd] = std::string("The '")+cmd+"' command prevents hist from writing its normal status messages to stderr.  It can still dump histograms to stdout if they are requested.";
  processSilent(args); // We don't need to do this here as, unusually, silency has to be done before the others except help.  But leaving it here serves as a reminder.

  cmd = "batch";
  os << " [" << cmd << "]\n";
  help[cmd] = std::string("The '")+cmd+"' command requests that hist run in a mode not requiring and X-display.  It imples 'quit'.  It is suitable for batch or scripting running.  In older versions of this program, it also implied 'dump /dev/stdout', but not any more.  Request dump explicitely now, if you require it.";
  {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), cmd);
    if (namePtr!=args.end()) {
      Configuration::batch = true;
      Configuration::quitOnEnd = true;
      std::mes() << "Will run in batch mode and quit when input data stream closes." << std::endl;
    }
  }
  /* os << " [name TITLE (deprecated in favour of title)]";
  if (true) {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), "name");
    if (namePtr!=args.end() && namePtr+1 !=args.end()) {
      //Configuration::noTitle=false;
      Configuration::title=(*(namePtr+1));
      Configuration::title=url_unescape(Configuration::title);
      std::mes() << "Histogram titled [" << Configuration::title<< "]" <<std::endl;
    }
  }*/
  /*
  os << " [function FUNCTION]";
  if (true) {
    std::vector<std::string>::const_iterator namePtr=find(args.begin(),args.end(), "function");
    if (namePtr!=args.end() && namePtr+1 !=args.end()) {
      formulaText=(*(namePtr+1));
      std::mes() << "Will superimpose the function: ["<< formulaText <<"]"<<std::endl;
    }
  }
  */



  if (Configuration::dim == 1) {
    if (Configuration::calclx || Configuration::calcux || Configuration::calcnx) {
      Configuration::autosizing = true;
    } else {
      Configuration::autosizing = false;
    }
  } else if (Configuration::dim==2) {
    if (Configuration::calclx || Configuration::calcux || Configuration::calcly || Configuration::calcuy || Configuration::calcnx || Configuration::calcny) {
      Configuration::autosizing = true;
      
    } else {
      Configuration::autosizing = false;
    }
  }

  if (Configuration::autosizing) {
      std::mes() << "AUTO-SIZING" << std::endl;
  } else {
      std::mes() << "Using FAST mode as no need to autosize!" << std::endl;
  }








  if ((!showHelp) && mustStop) {
    usage(os.str());
    exit(mustStop?1:0);
  }

  if (showHelp && showHelpCmd=="") {
    usage(os.str());
    exit(0);
  }

  if (showHelp && showHelpCmd!="") {
    std::map<std::string, std::string>::const_iterator it = help.find(showHelpCmd);
    if (it == help.end()) {
      std::mes() << "\nCould not find any help relating to ["<<showHelpCmd<<"]\n\n";
      usage(os.str());
      exit(1);
    } else {
      std::mes() << it->second << std::endl;
      exit(0);
    }
  }

}

void usage(const std::string & builtPart) {
  std::mes() << "\n" << builtPart << std::endl;
  std::mes() << "See examples at http://www.hep.phy.cam.ac.uk/~lester/hist/" << std::endl;
}


