
#ifndef LESTER_REDRAWER_H
#define LESTER_REDRAWER_H

#include <time.h>
#include <thread>
#include <mutex>

class Redrawer {
 private:
  Redrawer();
 public:
  static Redrawer & instance() {
    static Redrawer redrawer;
    return redrawer;
  }
  void handleRedrawsEtc();
 public:
  void bufferBuild();
 private:
  std::mutex m_mutex; // to stop two processes trying to do build buffers at the same time
  //bool m_redrawIsBeingProcessed;
  unsigned long m_lastBuilt;
  unsigned long m_nextInterestingDataSize;
  time_t m_lastBuildTime;
  bool m_everBuilt;
  void doStats() const;
};

#endif
