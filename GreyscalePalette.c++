
#include "GreyscalePalette.h"

GreyscalePalette::GreyscalePalette() {
  setInterpolant(0.00,Colour(1,1,1));
  setInterpolant(1.00,Colour(0,0,0));
}
