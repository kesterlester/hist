
#ifndef LESTER_THEPALETTE_H
#define LESTER_THEPALETTE_H

#include <memory>
#include "Palette.h"

class ThePalette {
  ThePalette();
 public:
  static std::shared_ptr<Palette> get();
  static void set(std::shared_ptr<Palette> pal) {
    s_palette = pal;
  }
  static void setRainbow(); // a convenience method
  static void setAllanach(); // a convenience method
  static void setGreyscale(); // a convenience method
  static void setUser(); // a convenience method
 private:
  static std::shared_ptr<Palette> s_palette;
};


#endif
