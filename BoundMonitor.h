#ifndef LESTER_BOUNDMONITOR_H
#define LESTER_BOUNDMONITOR_H

class BoundMonitor {
 public:
  BoundMonitor() : m_used(false) {}
  void note(const double x) {
    if (m_used) {
      if (x<m_min) {
	m_min=x;
      } else if (x>m_max) {
	m_max=x;
      }
    } else {
      m_max=m_min=x;
      m_used=true;
    }
  }
  void reset() { m_used=false; }
  bool isUsed() const { return m_used; }
  double max() const { return m_max; } // undefined if !isUsed()
  double min() const { return m_min; } // undefined if !isUsed()
  bool isSingular() const { return (!m_used) || (m_max==m_min); }
  double width() const { if (m_used) { return m_max-m_min; } else { return 0; }}
private:
  bool m_used;
  double m_min;
  double m_max;  
};

#endif
