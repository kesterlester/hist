
#ifndef LESTER_STATS2D_H
#define LESTER_STATS2D_H

#include "Stats.h"

class Stats2D {
public:
  Stats2D() : sumweight(0) {
  }
  Stats2D & note(const double x, const double y, const double weight=1) {
	  sumweight += weight;
	  m_stats_x.note(x,weight);
	  m_stats_y.note(y,weight);
	  return *this;
  }
  const Stats & x() const { return m_stats_x; }
  const Stats & y() const { return m_stats_y; }
  const double getSumWeight() const { return sumweight; }
private:
  Stats m_stats_x;
  Stats m_stats_y;
  double sumweight;
};

inline std::ostream & operator<<(std::ostream & os, const Stats2D & stats) {
  return os << "[x:" << stats.x() << ", y:" << stats.y() << "]"; 
}

#endif
