
#include "AllanachPalette.h"

AllanachPalette::AllanachPalette() {
  setInterpolant(0.00,Colour(0,0,0));
  setInterpolant(0.15,Colour(0,0,1));
  setInterpolant(0.35,Colour(1,0,1));
  setInterpolant(0.50,Colour(1,0,0));
  setInterpolant(1.00,Colour(1,1,0));
}
