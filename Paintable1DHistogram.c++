
#include "Paintable1DHistogram.h"
#include "TfmHistBox.h"
#include "TfmAxisText.h"
#include "PaintableAxes.h"
#include "PaintableTitle.h"
#include "PaintableLegend.h"
#include "Configuration.h"
#include "Hist1D.h"
#include "Colour.h"
#include "LogScaleHelper.h"
#include <iostream>

#include "LogHelper.h"

void Paintable1DHistogram::paint(cairo_t * c, std::ostream & os) const {

  if (m_hist1DPtrMap.empty()) {
    return;
  }

  TfmAxisText tat;
  std::vector<Data::Kind> types;
  std::vector<Colour> colours;

  double smallestWeightToPlot;
  double largestWeightToPlot;
  bool first=true;

  for (Data::Hist1DPtrMap::const_iterator it=m_hist1DPtrMap.begin();
       it != m_hist1DPtrMap.end(); ++it) {

    const Hist1D & hist = *(it->second);
    double tmp_smallestWeightToPlot;
    double tmp_largestWeightToPlot;
    findLargestAndSmallestWeightToPlot(true, hist, tmp_smallestWeightToPlot, tmp_largestWeightToPlot);
    if (first || (tmp_smallestWeightToPlot < smallestWeightToPlot)) {
      smallestWeightToPlot = tmp_smallestWeightToPlot;
    }
    if (first || (tmp_largestWeightToPlot > largestWeightToPlot)) {
      largestWeightToPlot = tmp_largestWeightToPlot;
    }
    first = false;
  }






  for (Data::Hist1DPtrMap::const_iterator it=m_hist1DPtrMap.begin();
       it != m_hist1DPtrMap.end(); ++it) {
    // Prepare for colurs, in case they are needed.
    types.push_back(it->first);
    int colour = 0;
    for (Data::Kinds::const_iterator jit = Data::kinds.begin();
            jit != Data::kinds.end();
            ++jit) {
        if ((*jit)==it->first) {
           break;
        }
        colour++; 
    }
    const Colour col = Colour::setColourFromIndex(colour);
    colours.push_back(col);

    // (1) Set co-ords up to match histogram
    const Hist1D & hist = *(it->second);

    TfmHistBox tfm(hist.xAxis.leftMostExtent(), adapt(smallestWeightToPlot),
                   hist.xAxis.rightMostExtent()  , adapt(largestWeightToPlot));

    if (c) tfm.paint(c,os);


    // (2) Fill in the "yellow" of the histogram

    if (c) cairo_move_to(c, hist.xAxis.leftMostExtent(), adapt(smallestWeightToPlot)); // not quite right.  Will be silly if eg liny=true, lx=-2, uy=-1.
    if (!c) {
      if (Configuration::multi) {
        hist.dumpTo(os, &((it->first)));
      } else {
        hist.dumpTo(os);
      }
    }
    for (unsigned int n=0; n<hist.xAxis.n; ++n) {

      const Bin & xBin = hist.xAxis.getBinByNumber(n);
      double weight = hist.weightForBinNumber(n);

      if (Configuration::haveLogScaleY && weight<=0) {
        weight=smallestWeightToPlot;
      }

      if (c) cairo_line_to(c, xBin.from , adapt(weight));
      if (c) cairo_line_to(c, xBin.to   , adapt(weight));
      //if (!c) {
      //  os << xBin.from << " " << xBin.to << " " << weight << "\n";
      //}
    }
    if (c) cairo_line_to(c, hist.xAxis.rightMostExtent(), adapt(smallestWeightToPlot));
    if (Configuration::dark) {
      if (c) cairo_set_source_rgb(c, 0, 0, 1); // blue
    } else {
      if (c) cairo_set_source_rgb(c, 1, 1, 0); // yellow
    }
    //Colour::setColourFromIndex(c, colour).paint(c,os);;
    if (!Configuration::multi) {
      if (c) cairo_fill_preserve(c);
    }


    /// (3) Draw a border round the yellow histogram data
    if (!Configuration::multi) {
      // non-multi
      if (c) Configuration::setFGColour(c);
    } else {
      // multi
      col.paint(c,os);
    }
    if (c) tat.paint(c,os);
    if (c) cairo_set_line_width(c,Configuration::graphThickness);
    if (c) cairo_stroke(c);

    // (3.5) Draw error bars if needed
    if (c) tfm.paint(c,os);
    if (Configuration::drawErrors) {

      if (!Configuration::multi) {
        // non-multi
        if (c) Configuration::setFGColour(c);
      } else {
        // multi
        col.paint(c,os);
      }
      for (unsigned int n=0; n<hist.xAxis.n; ++n) {

        const Bin & xBin = hist.xAxis.getBinByNumber(n);
        double weight = hist.weightForBinNumber(n);


        if (weight>0) {

          //if (c) cairo_set_line_width(c,(-xBin.from+xBin.to)/10);
          if (c) cairo_move_to(c, (xBin.from+xBin.to)*0.5     , adapt(weight+sqrt(fabs(weight)),smallestWeightToPlot   ));
          if (c) cairo_line_to(c, (xBin.from+xBin.to)*0.5 ,     adapt(weight-sqrt(fabs(weight)),smallestWeightToPlot   ));
          if (c) cairo_move_to(c, (xBin.from*.75+xBin.to*.25) , adapt(weight+sqrt(fabs(weight)),smallestWeightToPlot   ));
          if (c) cairo_line_to(c, (xBin.from*.25+xBin.to*.75) , adapt(weight+sqrt(fabs(weight)),smallestWeightToPlot   ));
          if (c) cairo_move_to(c, (xBin.from*.75+xBin.to*.25) , adapt(weight-sqrt(fabs(weight)),smallestWeightToPlot   ));
          if (c) cairo_line_to(c, (xBin.from*.25+xBin.to*.75) , adapt(weight-sqrt(fabs(weight)),smallestWeightToPlot   ));

        }
      }
    }
    if (c) tat.paint(c,os);
    if (c) cairo_set_line_width(c,Configuration::graphThickness);
    if (c) cairo_stroke(c);

  }

  // (4) Now draw axes.
  {
    const Hist1D & aHist = *(m_hist1DPtrMap.begin()->second);
    if (c) tat.paint(c,os);
    PaintableAxes axes(aHist.xAxis, Axis(10,smallestWeightToPlot,largestWeightToPlot,Configuration::haveLogScaleY, false));
    if (c) axes.paint(c,os);
  }

  // (5) Now draw title.
  if (Configuration::title != "") {
    PaintableTitle title(Configuration::title);
    if (c) title.paint(c,os);
  }

  if (Configuration::multi) {
    // (6) Now draw legend.
    if (Configuration::showLegend) {
      PaintableLegend legend(types, colours);
      if (c) legend.paint(c,os);
    }
  }

  if (c) cairo_identity_matrix(c);
  if (!c) {
    os << std::flush;
  }
}

