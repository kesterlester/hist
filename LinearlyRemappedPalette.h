
#ifndef LESTER_LINEARLYREMAPPEDPALETTE_H
#define LESTER_LINEARLYREMAPPEDPALETTE_H

#include "InterpolatedPalette.h"

#include <memory>

class LinearlyRemappedPalette : public Palette {
 public:
  LinearlyRemappedPalette(const double from1, // value in old palette
			  const double to1,   // value in new palette
			  const double from2,
			  const double to2,
			  const std::shared_ptr<Palette> pal) 
    : m_from1(from1),
      m_to1(to1),
      m_from2(from2),
      m_to2(to2),
      m_pal(pal) {
    if (!pal) {
      throw;
    }
  }
  Colour getColourFrom(const double weight) const {
    const double frac = (weight-m_to1)/(m_to2-m_to1);
    return m_pal->getColourFrom(  frac*m_from2 + (1-frac)*m_from1  );
  }
private:
  double m_from1;
  double m_to1;
  double m_from2;
  double m_to2;
  std::shared_ptr<Palette> m_pal;
};

#endif
