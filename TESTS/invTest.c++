
#include "../NonInvalidatingSequence.h"
#include <iostream>

int main() {
  

  {
    NonInvalidatingSequence<int> seq;
    seq.push_back(10);
    seq.push_back(11);
    seq.push_back(12);
    seq.push_back(13);
    seq.push_back(14);
    
    {
      NonInvalidatingSequence<int>::const_iterator it = seq.begin();
      
      for (int i=10; i<=14; (++it,++i)) {
	int jj = *it;
	std::cout << i << " ... " << jj << std::endl;
      }
      std::cout << std::endl;
    }
    
    {
      int i=10;
      for (NonInvalidatingSequence<int>::const_iterator it = seq.begin();
	   it != seq.end();
	   (++it,++i)) {
	int jj = *it;
	std::cout << i << " ... " << jj << std::endl;
      }
      std::cout << std::endl;
    }
  }


  {

    NonInvalidatingSequence<int> seq(2);

    {
      for (int i=1; i<=20; ++i) {
	seq.push_back(i);
      }
    }

    {
      int i=1;
      for (NonInvalidatingSequence<int>::const_iterator it = seq.begin();
	   it != seq.end();
	   (++it,++i)) {
	int jj = *it;
	std::cout << i << " ... " << jj << std::endl;
      }
      std::cout << std::endl;
    }

  }


  std::cout << "sizeof(double)=="<<sizeof(double) << std::endl;
  std::cout << "sizeof(float)=="<<sizeof(float) << std::endl;
  return 0;
}
