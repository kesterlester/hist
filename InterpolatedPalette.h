
#ifndef LESTER_INTERPOLATEDPALETTE_H
#define LESTER_INTERPOLATEDPALETTE_H

#include "Palette.h"
#include <map>

class InterpolatedPalette : public Palette {
 protected:
  InterpolatedPalette() {};
 public:
  void setInterpolant(const double weight, const Colour & colour);
  virtual Colour getColourFrom(const double weight) const;
 private:
  typedef std::map<double, Colour> Map;
  Map m_map;
};

#endif
