
#ifndef LESTER_COLOURSCALE_H
#define LESTER_COLOURSCALE_H

#include "CairoPaintable.h"

class Palette;

#include <memory>

class ColourScale : public CairoPaintable {
public:
  ColourScale(const double weightFrom,
	      const double weightTo,
	      const bool logz,
	      const std::shared_ptr<Palette> pal);
  virtual void paint(cairo_t * c, std::ostream & os) const;
private:
  ColourScale(); // deliberately not implemented
private:
  double from;
  double to;
  bool logz;
  std::shared_ptr<Palette> pal;
  double effectiveTo;
  double effectiveFrom;
};

#endif
