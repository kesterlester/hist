
#include "ColourScale.h"

#include "PaintableAxes.h"
#include <cmath>
#include "stringFrom.h"
#include "Palette.h"
#include "Configuration.h"

ColourScale::ColourScale(const double weightFrom,
			 const double weightTo,
			 const bool logz,
			 const std::shared_ptr<Palette> pal) : 
  from(weightFrom),
  to(weightTo),
  logz(logz),
  pal(pal) {
  effectiveFrom = (logz ? log(from)/log(10.) : from);
  effectiveTo = (logz ? log(to)/log(10.) : to);
  // std::cerr << "Colour scale requested from " << weightFrom << " to " << weightTo << std::endl;
}


void ColourScale::paint(cairo_t * c, std::ostream & os) const {
  if (c) {
  const int n=100;
  for(int i=0; i<n; ++i) {

    const double lambdaW = (0.5+i)/n;
    const double effectiveWeight=lambdaW*effectiveTo + (1.-lambdaW)*effectiveFrom; // actualWeight = (logz ? exp(effectiveWeight) : effectiveWeight);

    const double lambdaL = static_cast<double>(i+0)/n;
    const double lambdaU = static_cast<double>(i+1)/n;
 
    //std::mes() << "fr " << from << " to " << to << " www " << weight << " col " << ThePalette::get()->getColourFrom(effectiveWeight) << std::endl;
    //std::cerr << "Doing colourscale with col  for " << effectiveWeight << " being " << << std::endl;
    pal->getColourFrom(effectiveWeight).paint(c,os);
    cairo_move_to(c, 1.05, lambdaL);
    cairo_line_to(c, 1.10, lambdaL);
    cairo_line_to(c, 1.10, lambdaU);
    cairo_line_to(c, 1.05, lambdaU);
    cairo_fill(c);
  }
  Configuration::setFGColour(c);
  cairo_move_to(c, 1.05, 0);
  cairo_line_to(c, 1.10, 0);
  cairo_line_to(c, 1.10, 1);
  cairo_line_to(c, 1.05, 1);
  cairo_close_path(c);
  cairo_stroke(c);

  // label the scale
  std::list<double> placesToLabel = PaintableAxes::placesToLabel(Axis(1, effectiveFrom, effectiveTo, false, false));

  cairo_text_extents_t CTE;

  for (std::list<double>::const_iterator it = placesToLabel.begin();
       it != placesToLabel.end();
       ++it) {
    const double effVal = *it;
    const std::string label = (logz ? std::string("10^")+stringFrom(effVal) : stringFrom(effVal));
    cairo_text_extents(c, label.c_str(), &CTE);
 
    const double yTickPos = (effVal-effectiveFrom)/(effectiveTo - effectiveFrom);
    
      cairo_move_to(c, 1.11, yTickPos-CTE.height/2.0);
      cairo_show_text(c, label.c_str());

  }
  }
}
