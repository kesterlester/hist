
#ifndef LESTER_PAINTABLEHISTOGRAM_H
#define LESTER_PAINTABLEHISTOGRAM_H

#include "CairoPaintable.h"
#include <memory>

//fwd dec
class Hist1D;

class PaintableHistogram : public CairoPaintable {
 public: 
  /*  virtual void paint(cairo_t * c) const {
    paintOrDump(c, std::mes());
  };
  void dump(std::ostream & os) const {
    paintOrDump(0, os);
  };
  */
  //virtual void paintOrDump(cairo_t * c, std::ostream & os) const = 0;
  virtual ~PaintableHistogram() {};
};

#endif
