
#include "PaintableLegend.h"
#include "TfmAxisText.h"

void PaintableLegend::paint(cairo_t * c, std::ostream & os) const {
  if (c) {
  TfmAxisText tst;
  tst.paint(c, os);

  cairo_font_extents_t CFE;
  cairo_font_extents(c, &CFE);
  cairo_text_extents_t CTE;

  for (unsigned int i=0; i<m_types.size(); ++i) {
    cairo_text_extents(c, m_types[i].c_str(), &CTE);  
    cairo_move_to(c, 0.05, 1.0 - CFE.height*(i+1));
    m_colours[i].paint(c,os);
    cairo_show_text(c, m_types[i].c_str());
  }
  
  }
} 
